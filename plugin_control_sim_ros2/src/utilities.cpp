#include "utilities.h"

geometry_msgs::msg::Quaternion toQuaternion(float yaw, float pitch, float roll)
{
    auto cy = static_cast<float>(cos(yaw * 0.5));
    auto sy = static_cast<float>(sin(yaw * 0.5));
    auto cp = static_cast<float>(cos(pitch * 0.5));
    auto sp = static_cast<float>(sin(pitch * 0.5));
    auto cr = static_cast<float>(cos(roll * 0.5));
    auto sr = static_cast<float>(sin(roll * 0.5));

    geometry_msgs::msg::Quaternion q;
    q.w = cr * cp * cy + sr * sp * sy;
    q.x = sr * cp * cy - cr * sp * sy;
    q.y = cr * sp * cy + sr * cp * sy;
    q.z = cr * cp * sy - sr * sp * cy;

    return q;
}

geometry_msgs::msg::Pose getRandomPose(const float x1, const float y1, const float x2, const float y2, float z)
{
    geometry_msgs::msg::Pose pose;

    float x = getRandomFloat(x1, x2);
    float y = getRandomFloat(y1, y2);

    float r = sqrt(x * x + y * y);

    while (r < 0.4)
    {
        x = getRandomFloat(x1, x2);
        y = getRandomFloat(y1, y2);
        r = sqrt(x * x + y * y);
    }

    float roll = getRandomFloat(0, 2 * M_PI), pitch = getRandomFloat(0, M_PI), yaw = getRandomFloat(0, M_PI);

    pose.orientation = toQuaternion(yaw, pitch, roll);
    pose.position.x = x;
    pose.position.y = y;
    pose.position.z = z;

    return pose;
}

geometry_msgs::msg::Point getRandomPosition(const float x1, const float y1, const float x2, const float y2, float z)
{
    geometry_msgs::msg::Point position;
    float x = getRandomFloat(x1, x2);
    float y = getRandomFloat(y1, y2);

    float r = sqrt(x * x + y * y);

    while (r < 0.4)
    {
        x = getRandomFloat(x1, x2);
        y = getRandomFloat(y1, y2);
        r = sqrt(x * x + y * y);
    }
    position.x = x;
    position.y = y;
    position.z = z;
    return position;
}

geometry_msgs::msg::Quaternion getRandomQuaternion()
{
    geometry_msgs::msg::Quaternion orientation;
    float roll = getRandomFloat(0, 2 * M_PI), pitch = getRandomFloat(0, M_PI), yaw = getRandomFloat(0, M_PI);

    orientation = toQuaternion(yaw, pitch, roll);
    return orientation;
}

float getRandomFloat(const float lower_bound, const float upper_bound)
{
    std::uniform_real_distribution<float> unif(lower_bound, upper_bound);
    std::random_device rand_dev;
    std::mt19937 rand_engine(rand_dev());
    float x = unif(rand_engine);
    return x;
}