#!/bin/bash

ros2 service call /control_coppelia_brain/spawn_items custom_interfaces/srv/SpawnItem "{ \
    random_type: true, \
    random_pose: true, \
    amount: {data: 3}}"