#!/bin/bash

ros2 service call /control_coppelia_camera/spawn_items custom_interfaces/srv/SpawnItem "{\
    pose: {position: {x: 0.5,y: 0.5,z: 0.7}, orientation: {w: 1.0,x: 0.0,y: 0.0,z: 0.0}},
    type: {data: plate},\
    random_type: false,\
    random_position: true,\
    random_orientation: false,
    amount: {data: 1}}"