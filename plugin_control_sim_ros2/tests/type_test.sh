#!/bin/bash

ros2 service call /control_sim_ros_2/spawn_items custom_interfaces/srv/SpawnItem "{ \
    type: {data: orange}, \
    random_type: false, \
    random_pose: true, \
    amount: {data: 3}}"