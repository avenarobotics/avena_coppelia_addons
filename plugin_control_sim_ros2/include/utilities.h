#pragma once

#include <chrono>
#include <geometry_msgs/msg/pose.hpp>
#include <geometry_msgs/msg/quaternion.hpp>
#include <geometry_msgs/msg/point.hpp>
// #include <utility>
#include <random>
#include <iostream>

typedef std::chrono::high_resolution_clock::time_point time_var;

#define duration(a) std::chrono::duration_cast<std::chrono::milliseconds>(a).count()
#define timeNow() std::chrono::high_resolution_clock::now()

geometry_msgs::msg::Pose getRandomPose(const float x1, const float y1, const float x2, const float y2, float z);
geometry_msgs::msg::Point getRandomPosition(const float x1, const float y1, const float x2, const float y2, float z);
geometry_msgs::msg::Quaternion getRandomQuaternion();

float getRandomFloat(const float lower_bound, const float upper_bound);