#ifndef STRUCTS_ENUMS_HPP
#define STRUCTS_ENUMS_HPP
//____________CPP______________
#include <map>
//_________Eigen___________________
#include <Eigen/Core>
//____________ROS_________________________-
//___msgs_____
#include "custom_interfaces/msg/items.hpp"
#include "custom_interfaces/msg/occupancy_grid.hpp"
#include "custom_interfaces/msg/grasp.hpp"
#include "custom_interfaces/msg/place.hpp"

namespace place
{
    // _________global declarations___________
    using Items = custom_interfaces::msg::Items;
    using Grasp = custom_interfaces::msg::Grasp;
    using OccGrid = custom_interfaces::msg::OccupancyGrid;
    using PlaceMsg = custom_interfaces::msg::Place;

    // _________structs and enum
    struct InputMsg
    {
        Items items_msg;
        // Grasp grasp_msg;
        OccGrid occ_grid_msg;
    };

    struct Params
    {
        std::map<std::string,
                 std::map<std::string, Eigen::Vector3f>>
            table_areas_coords;
        float occ_grid_leaf_size;
        float item_rotation_angle;
    };
    struct Goal
    {
        std::vector<std::string> selected_areas_labels;
        std::string selected_area_operator;
        float search_shift;
        uint8_t no_of_required_place_positions;
        Grasp grasp_msg;
        int32_t selected_item_id;
    };
    struct CoppeliaScene
    {
        inline static const std::string scene_name = "scene";
        inline static const std::string world_name = "DummyWorld";
        inline static const std::string robot_name = "panda_ghost";
        inline static const std::string robot_ik = "Panda_ik";
        inline static const std::string target_name = "target_ghost";
        inline static const std::string hand = "Panda_gripper_attach_point_ghost";
        inline static const std::string collision = "collisions_parent";
        inline static const size_t panda_dofs = 7;
    };
    enum Status_e : size_t
    {
        SUCCESS = EXIT_SUCCESS,
        FAILURE,
        INVALID,
        TIME_OUT
    };
    enum Area_e : size_t
    {
        SECURITY_AREA,
        TOOLS_AREA,
        ITEM_AREA,
        OPERATING_AREA
    };
    enum AreaOperator_e : size_t
    {
        INCLUDE,
        EXCLUDE
    };
    enum IKResult_e : size_t
    {
        IK_NOT_PERFORMED,
        IK_SUCCESS,
        IK_FAILURE
    };
    enum OccGrid_e : size_t
    {
        EMPTY = 0,     // black
        OCCUPIED = 255 // white
    };

} // namespace place
#endif