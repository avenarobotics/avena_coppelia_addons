#include "place.hpp"
using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::milliseconds;
namespace place
{
    Place::Place(rclcpp::Logger logger, bool debug) : _logger(logger), _debug(debug) {}
    //______Algorithm_________________
    int Place::execute(const std::shared_ptr<const InputMsg> input_msg, const Params &params, const Goal &goal, std::shared_ptr<PlaceMsg> output_msg)
    {
        // helpers::TimerROS(__func__,_logger);
        cv::Mat occ_grid;
        helpers::converters::rosImageToCV(input_msg->occ_grid_msg.occupancy_grid_ros, occ_grid);
        std::vector<std::vector<Eigen::Vector3f>> target_areas_coords;
        if (_getTargetAreasCoords(params.table_areas_coords,
                                  goal.selected_areas_labels,
                                  goal.selected_area_operator,
                                  target_areas_coords) != Status_e::SUCCESS)
        {
            return Status_e::FAILURE;
        }
        // for (auto &area : target_areas_coords)
        // {
        //     for (auto &pt : area)
        //     {
        //         RCLCPP_INFO_STREAM(_logger, "table area coordinates: \n"
        //                                         << pt);
        //     }
        // }
        pcl::PointCloud<pcl::PointXYZ>::Ptr item_in_hand_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
        if (_getItemInHandPtcld(input_msg->items_msg.items,
                                goal.selected_item_id,
                                item_in_hand_ptcld) != Status_e::SUCCESS)
        {
            return Status_e::FAILURE;
        }
        Eigen::Vector3f initial_item_positon;
        Eigen::Vector2f initial_obb_dims;
        _computeObbDimsAndPosition(item_in_hand_ptcld, initial_obb_dims, initial_item_positon);
        if (_loadSceneData() != Status_e::SUCCESS)
        {
            return Status_e::FAILURE;
        }
        json places_log;
        int place_proposals_amount = M_PI * 2 / params.item_rotation_angle;
        output_msg->place_poses.resize(goal.grasp_msg.grasp_poses.size());
        // for each grasp: compute specific no of place positions
        for (size_t grasp_pose_idx = 0; grasp_pose_idx < goal.grasp_msg.grasp_poses.size(); grasp_pose_idx++)
        {
            // for each area: search for set of place poses
            for (auto &target_area_coords : target_areas_coords)
            {
                Eigen::Vector3f target_area_min(target_area_coords[0]), target_area_max(target_area_coords[1]);

                auto [padded_height, padded_width] = _getSearchLimits(target_area_coords, goal.search_shift, initial_obb_dims);
                float top_corner_x = target_area_max.x(); // top
                float top_corner_y;
                for (int idx_x = 0; idx_x < padded_height; ++idx_x)
                {
                    top_corner_y = target_area_max.y(); // left
                    for (int idx_y = 0; idx_y < padded_width; ++idx_y)
                    {
                        // RCLCPP_INFO_STREAM(_logger, "moving upper corner y: " << top_corner_y);
                        // RCLCPP_INFO_STREAM(_logger, "moving lower corner x: " << top_corner_x);
                        std::vector<Eigen::Affine3f> place_poses;
                        std::string search_id = std::to_string(idx_x) + " , " + std::to_string(idx_y);
                        for (int i = 0; i <= place_proposals_amount; i++)
                        {
                            pcl::PointCloud<pcl::PointXYZ>::Ptr rotated_item_in_hand_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
                            rotated_item_in_hand_ptcld = _rotateItem(item_in_hand_ptcld, i * params.item_rotation_angle);
                            Eigen::Vector3f obb_center_position;
                            Eigen::Vector2f obb_dims;
                            if (_computeObbDimsAndPosition(rotated_item_in_hand_ptcld, obb_dims, obb_center_position) != Status_e::SUCCESS)
                            {
                                return Status_e::FAILURE;
                            }
                            Eigen::Vector3f place_position = {top_corner_x - obb_dims.x() / 2, top_corner_y - obb_dims.y() / 2, 0.0};
                            // RCLCPP_INFO_STREAM(_logger, "proposed grid place position: " << place_position.x() << " , " << place_position.y() << " , " << place_position.z());
                            Eigen::Affine3f transform(Eigen::Affine3f::Identity());
                            transform.translation() = place_position;
                            pcl::transformPointCloud(*rotated_item_in_hand_ptcld, *rotated_item_in_hand_ptcld, transform);
                            cv::Mat item_img(cv::Mat::zeros(occ_grid.rows, occ_grid.cols, CV_8UC1));
                            _getImageFromPtcld(*rotated_item_in_hand_ptcld, params.occ_grid_leaf_size, item_img);
                            if (_checkFreeSpace(item_img, place_position, obb_dims, occ_grid, params.occ_grid_leaf_size) == Status_e::SUCCESS)
                            {
                                // RCLCPP_INFO_STREAM(_logger, "proposed grid place position is free of collision");
                                Eigen::Affine3f place_pose = _rotateGripperToPlace(goal.grasp_msg.grasp_poses[grasp_pose_idx].grasp_pose, place_position, initial_item_positon);
                                // for collision with table
                                place_pose.translation().z() = 0.0;
                                place_position.z() = goal.grasp_msg.grasp_poses[grasp_pose_idx].grasp_pose.position.z+0.01;
                                // return place pose from world frame
                                place_pose.translation() += place_position;
                                // RCLCPP_INFO_STREAM(_logger, "proposed gripper place position in world frame: " << place_pose.translation());
                                if (_checkGrasp(place_pose, goal.selected_item_id, goal.grasp_msg.grasp_poses[grasp_pose_idx]) == Status_e::SUCCESS)
                                {
                                    // RCLCPP_INFO_STREAM(_logger, "place trial no#: " << i << " passed IK");
                                    place_poses.push_back(place_pose);
                                    if (place_poses.size() >= static_cast<size_t>(goal.no_of_required_place_positions))
                                    {
                                        // RCLCPP_INFO_STREAM(_logger, "found " << place_poses.size() << " place poses in the search step");
                                        custom_interfaces::msg::PlaceData place_pose_saving;
                                        for (auto &pose : place_poses)
                                        {
                                            geometry_msgs::msg::Pose ros_pose;
                                            helpers::converters::eigenAffineToGeometry(pose, ros_pose);
                                            place_pose_saving.poses.push_back(ros_pose);
                                        }
                                        output_msg->place_poses.push_back(place_pose_saving);
                                    }
                                }
                            }
                            // else
                            //     RCLCPP_WARN_STREAM(_logger, "proposed grid place position is in collision");
                        } // end of place proposals
                        // RCLCPP_WARN_STREAM(_logger, "failed to find all place poses in one search step");
                        place_poses.clear();
                        top_corner_y -= goal.search_shift;
                    } // target area y search
                    top_corner_x -= goal.search_shift;
                } // target area x search

                // std::ofstream place_log_file("/root/ros2_ws/src/avena_coppelia_addons/plugin_place/place_log.json");
                // place_log_file << places_log;
                RCLCPP_WARN(_logger, "Goal failed for current area, proceeding to next one");
            }
            if (output_msg->place_poses.size() > 0)
            {
                RCLCPP_INFO(_logger, "Found  %i place positions ", output_msg->place_poses.size());
                return Status_e::SUCCESS;
            }
            // end of areas
            // // only one grasp: No need for for loop, it is left for future for list
            // return Status_e::FAILURE;
        } // end of grasps

        RCLCPP_WARN(_logger, "Goal failed for current area, proceeding to next one");
        return Status_e::FAILURE;
    }
    int Place::_getTargetAreasCoords(const std::map<std::string,
                                                    std::map<std::string, Eigen::Vector3f>>
                                         table_areas_coords,
                                     const std::vector<std::string> selected_areas_labels,
                                     const std::string selected_area_operator,
                                     std::vector<std::vector<Eigen::Vector3f>> &out_target_areas_coords)
    {
        helpers::Timer(__func__);
        RCLCPP_INFO_STREAM(_logger, __func__ << " started");
        if (selected_area_operator == _TableAreasOperators.at(AreaOperator_e::INCLUDE))
        {
            for (auto &area_label : selected_areas_labels)
            {
                if (area_label == _WholeTable)
                {
                    out_target_areas_coords.push_back({table_areas_coords.at("security_area").at("min_inter"), table_areas_coords.at("security_area").at("max_inter")});
                    return Status_e::SUCCESS;
                }
                else if (area_label != _TableAreasNames.at(Area_e::SECURITY_AREA))
                {
                    out_target_areas_coords.push_back({table_areas_coords.at(area_label).at("min"), table_areas_coords.at(area_label).at("max")});
                }
                else
                {
                    RCLCPP_WARN(_logger, "You should not pass security area as target area");
                }
            }
        }
        else if (selected_area_operator == _TableAreasOperators.at(AreaOperator_e::EXCLUDE))
        {
            std::map<std::string,
                     std::map<std::string, Eigen::Vector3f>>
                available_table_areas_coords(table_areas_coords);

            available_table_areas_coords.erase(_TableAreasNames.at(Area_e::SECURITY_AREA));

            for (auto &area_label : selected_areas_labels)
            {
                if (available_table_areas_coords.size() != 0)
                    available_table_areas_coords.erase(area_label);
            }

            if (available_table_areas_coords.size() != 0)
            {
                for (auto &area_coords : available_table_areas_coords)
                {
                    out_target_areas_coords.push_back({area_coords.second["min"], area_coords.second["max"]});
                }
            }
            else
            {
                RCLCPP_ERROR(_logger, "Area vector is empty(You probably excluded all areas), Goal Failed");
                return Status_e::INVALID;
            }
        }
        else
        {
            RCLCPP_ERROR(_logger, "Unsupported area operator");
            return Status_e::INVALID;
        }
        if (out_target_areas_coords.size() != 0)
            return Status_e::SUCCESS;
        else
            return Status_e::FAILURE;
    }
    int Place::_getItemInHandPtcld(const std::vector<Item> &items, const int &item_in_hand_id, pcl::PointCloud<pcl::PointXYZ>::Ptr out_item_in_hand_ptcld)
    {
        // helpers::Timer(__func__);
        // RCLCPP_INFO_STREAM(_logger, __func__ << " started");
        pcl::PointCloud<pcl::PointXYZ>::Ptr item_element_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
        auto item_result_it = std::find_if(items.begin(), items.end(),
                                           [item_in_hand_id](custom_interfaces::msg::Item item) {
                                               return item.id == item_in_hand_id;
                                           });
        if (item_result_it != items.end())
        {
            for (auto &item : items)
            {
                if (item.id == item_in_hand_id)
                {
                    for (auto &item_element : item.item_elements)
                    {
                        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element.merged_ptcld, item_element_ptcld);
                        *out_item_in_hand_ptcld += *item_element_ptcld;
                        item_element_ptcld->clear();
                    }
                }
            }
            helpers::commons::passThroughFilter(out_item_in_hand_ptcld, "z", -0.005, 0.005, true);
            helpers::commons::statisticalOutlierRemovalFilter(out_item_in_hand_ptcld);
            if (out_item_in_hand_ptcld->points.size() == 0)
            {
                RCLCPP_WARN(_logger, "Item data pointcloud is empty ");
                return Status_e::INVALID;
            }
            return Status_e::SUCCESS;
        }
        else
        {
            RCLCPP_ERROR(_logger, "Item in hand does exist in input message");
            return Status_e::FAILURE;
        }
    }
    int Place::_checkFreeSpace(const cv::Mat &item_img, const Eigen::Vector3f &place_position, const Eigen::Vector2f &obb_dims, const cv::Mat &occ_grid, const float &leaf_size)
    {
        // helpers::Timer(__func__);
        // RCLCPP_INFO_STREAM(_logger, __func__ << " started");
        // transform aabb from world to image frame and get index in image
        size_t bottom_x = floor((place_position.x() - obb_dims.x() / 2) / leaf_size);
        size_t bottom_y = std::abs((1 + (place_position.y() - obb_dims.y() / 2)) / leaf_size);
        size_t top_x = floor((place_position.x() + obb_dims.x() / 2) / leaf_size);
        size_t top_y = floor((1 + (place_position.y() + obb_dims.y() / 2)) / leaf_size);

        // _drawBorder(occ_grid, bottom_x, bottom_y, top_x, top_y);
        size_t zero = 0;
        auto clip = [](const size_t n, const size_t lower, const size_t upper) {
            return std::max(lower, std::min(n, upper));
        };
        bottom_x = clip(bottom_x, zero, static_cast<size_t>(occ_grid.rows));
        bottom_y = clip(bottom_y, zero, static_cast<size_t>(occ_grid.cols));
        top_x = clip(top_x, zero, static_cast<size_t>(occ_grid.rows));
        top_y = clip(top_y, zero, static_cast<size_t>(occ_grid.cols));
        cv::Rect roi(bottom_y, bottom_x, top_y - bottom_y, top_x - bottom_x);
        cv::Mat place_img = occ_grid(roi);
        cv::Mat item_in_place_img = item_img(roi);
        cv::Mat item_collsion;
        cv::bitwise_and(place_img, item_in_place_img, item_collsion);
        // static int img_idx = 0;
        // cv::imwrite("/root/ros2_ws/src/avena_coppelia_addons/plugin_place/images/occ_grid" + std::to_string(img_idx) + ".png", occ_grid);
        // cv::imwrite("/root/ros2_ws/src/avena_coppelia_addons/plugin_place/images/place_img" + std::to_string(img_idx) + ".png", place_img);
        // cv::imwrite("/root/ros2_ws/src/avena_coppelia_addons/plugin_place/images/item_img" + std::to_string(img_idx) + ".png", item_img);
        // cv::imwrite("/root/ros2_ws/src/avena_coppelia_addons/plugin_place/images/item_in_place_img" + std::to_string(img_idx) + ".png", item_in_place_img);
        // cv::imwrite("/root/ros2_ws/src/avena_coppelia_addons/plugin_place/images/item_collsion" + std::to_string(img_idx) + ".png", item_collsion);
        // img_idx++;

        // _showManyImages(__func__, 5, occ_grid, place_img, item_img, item_in_place_img, item_collsion);
        if (cv::countNonZero(item_collsion) > 0)
        {
            return Status_e::FAILURE;
        }
        else
        {
            return Status_e::SUCCESS;
        }
    }
    int Place::_computeObbDimsAndPosition(pcl::PointCloud<pcl::PointXYZ>::Ptr item_in_hand_ptcld, Eigen::Vector2f &out_obb_dims, Eigen::Vector3f &out_obb_center_position)
    {
        // helpers::Timer(__func__);
        out_obb_center_position = Eigen::Vector3f::Zero();
        out_obb_dims = Eigen::Vector2f::Zero();
        // helpers::Timer(__func__);
        Eigen::Vector4f centroid;
        pcl::compute3DCentroid(*item_in_hand_ptcld, centroid);
        Eigen::Matrix3f covariance;
        computeCovarianceMatrixNormalized(*item_in_hand_ptcld, centroid, covariance);
        Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
        Eigen::Matrix3f eigDx = eigen_solver.eigenvectors();
        eigDx.col(2) = eigDx.col(0).cross(eigDx.col(1));
        Eigen::Matrix4f p2w(Eigen::Matrix4f::Identity());
        p2w.block<3, 3>(0, 0) = eigDx.transpose();
        p2w.block<3, 1>(0, 3) = -1.f * (p2w.block<3, 3>(0, 0) * centroid.head<3>());
        pcl::PointCloud<pcl::PointXYZ> cPoints;
        pcl::transformPointCloud(*item_in_hand_ptcld, cPoints, p2w);
        pcl::PointXYZ min_pt, max_pt;
        pcl::getMinMax3D(cPoints, min_pt, max_pt);
        const Eigen::Vector3f mean_diag = 0.5f * (max_pt.getVector3fMap() + min_pt.getVector3fMap());
        // final transform
        const Eigen::Quaternionf qfinal(eigDx);
        const Eigen::Vector3f tfinal = eigDx * mean_diag + centroid.head<3>();

        out_obb_dims = {abs(max_pt.y - min_pt.y), abs(max_pt.z - min_pt.z)};
        out_obb_center_position = tfinal;

        return Status_e::SUCCESS;
    }
    Eigen::Affine3f Place::_rotateGripperToPlace(const geometry_msgs::msg::Pose &grasp_pose, const Eigen::Vector3f &place_position, const Eigen::Vector3f &initial_item_position)
    {
        Eigen::Affine3f grasp_pose_mat;
        helpers::converters::geometryToEigenAffine(grasp_pose, grasp_pose_mat);
        Eigen::Vector3f grasp_in_item_frame_vec = grasp_pose_mat.translation() - initial_item_position;
        Eigen::Affine3f grasp_pose_item_frame = Eigen::Translation3f(grasp_in_item_frame_vec) * Eigen::Quaternionf(grasp_pose_mat.rotation());
        Eigen::Vector3f grasp_in_item_frame_vec_flattended = grasp_in_item_frame_vec;
        grasp_in_item_frame_vec_flattended(2) = 0.0;
        Eigen::Quaternionf grasp_to_place_angle;
        grasp_to_place_angle.setFromTwoVectors(grasp_in_item_frame_vec_flattended, place_position);
        Eigen::Affine3f initial_place = grasp_pose_item_frame;
        initial_place = grasp_to_place_angle * initial_place;
        return Eigen::Quaternionf(helpers::commons::assignRotationMatrixAroundZ(M_PI)) * initial_place;
    }
    int Place::_checkGrasp(Eigen::Affine3f &place_pose, const int &selected_item_id, const custom_interfaces::msg::GraspData &grasp)
    {
        int target = simGetObjectHandle(CoppeliaScene::target_name.c_str());
        int world = simGetObjectHandle(CoppeliaScene::world_name.c_str());
        int ik_handle = simGetIkGroupHandle(CoppeliaScene::robot_ik.c_str());
        std::vector<int> panda_collision{simGetCollectionHandle(CoppeliaScene::robot_name.c_str()),
                                         simGetCollectionHandle(CoppeliaScene::scene_name.c_str())};
        std::vector<int> joint_names;
        for (size_t joint_idx = 1; joint_idx <= CoppeliaScene::panda_dofs; joint_idx++)
        {
            std::string joint_name = "joint_" + std::to_string(joint_idx) + "_ghost";
            int joint = simGetObjectHandle(joint_name.c_str());
            joint_names.emplace_back(joint);
        }
        std::pair<Eigen::Affine3f, int> pair;
        std::vector<float> joint_positions(CoppeliaScene::panda_dofs);
        Eigen::Vector3f vec = place_pose.translation();
        Eigen::Quaternionf quat(place_pose.rotation());
        Eigen::Quaternionf quat_pick(grasp.grasp_pose.orientation.w,
                                     grasp.grasp_pose.orientation.x,
                                     grasp.grasp_pose.orientation.y,
                                     grasp.grasp_pose.orientation.z);

        // pick target
        std::vector<float> position_pick = {static_cast<float>(grasp.grasp_pose.position.x),
                                            static_cast<float>(grasp.grasp_pose.position.y),
                                            static_cast<float>(grasp.grasp_pose.position.z)};
        std::vector<float> orientation_pick = {quat_pick.w(), quat_pick.x(), quat_pick.y(), quat_pick.z()};

        // place target
        std::vector<float> position_place = {vec.x(), vec.y(), vec.z() + 0.01f};
        std::vector<float> orientation_place = {quat.w(), quat.x(), quat.y(), quat.z()};

        // set pick target
        simSetObjectPosition(target, world, position_pick.data());
        simSetObjectQuaternion(target + sim_handleflag_wxyzquaternion, world, orientation_pick.data());

        // find config for pick pose
        int ik_result = simGetConfigForTipPose(ik_handle, CoppeliaScene::panda_dofs, joint_names.data(), 0.65, 100,
                                               joint_positions.data(), nullptr, 1, panda_collision.data(),
                                               nullptr, nullptr, nullptr, nullptr);

        // if ok go to that pose
        if (ik_result == IKResult_e::IK_SUCCESS)
        {
            for (size_t joint_idx = 0; joint_idx < CoppeliaScene::panda_dofs; joint_idx++)
                simSetJointPosition(joint_names[joint_idx], joint_positions[joint_idx]);
        }
        else
        {
            RCLCPP_ERROR(_logger, "Cannot find pose for grasp!");
            return Status_e::FAILURE;
        }

        // get object handle id
        int _hand = simGetObjectHandle(CoppeliaScene::hand.c_str());
        int _parent_handle = simGetObjectHandle(CoppeliaScene::collision.c_str());
        int g_selected_item_id = selected_item_id;
        int g_selected_item_handle = 0;
        std::string str_look_for = "_" + std::to_string(g_selected_item_id);
        int object_count;
        int *returned_handles = simGetObjectsInTree(_parent_handle, sim_handle_all, 2, &object_count);
        std::vector<int> objects = std::vector<int>(returned_handles, returned_handles + object_count);
        for (size_t i = 0; i < objects.size(); i++)
        {
            std::string name = simGetObjectName(objects[i]);
            size_t res = name.find(str_look_for);
            if (res != std::string::npos)
            {
                g_selected_item_handle = objects[i];
            }
        }
        if (g_selected_item_handle == 0)
        {
            RCLCPP_ERROR(_logger, "Invalid item handle, aborting");
            return Status_e::FAILURE;
        }

        // save object matrix to restore after place calculation
        std::vector<float> g_item_matrix(16);
        simGetObjectMatrix(g_selected_item_handle, world, g_item_matrix.data());

        // attach the object for calcualtions
        int result = simSetObjectParent(g_selected_item_handle, _hand, true);
        if (result == -1)
        {
            RCLCPP_ERROR(_logger, "Object attach failed");
        }

        // set place target
        simSetObjectPosition(target, world, position_place.data());
        simSetObjectQuaternion(target + sim_handleflag_wxyzquaternion, world, orientation_place.data());

        // find config for place target
        int place_result = simGetConfigForTipPose(ik_handle, CoppeliaScene::panda_dofs, joint_names.data(), 0.65, 100,
                                                  joint_positions.data(), nullptr, 1, panda_collision.data(),
                                                  nullptr, nullptr, nullptr, nullptr);

        // if ok go to that pose
        if (place_result == IKResult_e::IK_SUCCESS)
        {
            for (size_t joint_idx = 0; joint_idx < CoppeliaScene::panda_dofs; joint_idx++)
                simSetJointPosition(joint_names[joint_idx], joint_positions[joint_idx]);
        }

        // detach object
        result = simSetObjectParent(g_selected_item_handle, _parent_handle, true);
        if (result == -1)
        {
            RCLCPP_INFO(_logger, "Detaching failed ");
        }
        // restore object
        result = simSetObjectMatrix(g_selected_item_handle, world, g_item_matrix.data());
        if (result == -1)
        {
            RCLCPP_INFO(_logger, "Restoring item failed ");
        }

        // return success
        if (place_result == IKResult_e::IK_SUCCESS)
            return Status_e::SUCCESS;
        else
            return Status_e::FAILURE;

        return Status_e::FAILURE;
    }
    int Place::_loadSceneData()
    {
        helpers::Timer(__func__);
        RCLCPP_INFO_STREAM(_logger, __func__ << " started");
        std::vector<int> joints;
        for (size_t joint_idx = 1; joint_idx <= CoppeliaScene::panda_dofs; joint_idx++)
        {
            std::string joint_name = "joint_" + std::to_string(joint_idx) + "_ghost";
            int joint = simGetObjectHandle(joint_name.c_str());
            joints.push_back(joint);
        }
        if (joints.size() != 0)
        {
            return Status_e::SUCCESS;
        }
        else
        {
            return Status_e::FAILURE;
        }
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr Place::_rotateItem(const pcl::PointCloud<pcl::PointXYZ>::Ptr item_in_hand_ptcld, const float &rotation_angle)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr rotated_item_in_hand_ptcld(item_in_hand_ptcld);
        Eigen::Affine3f transform(Eigen::Affine3f::Identity());
        Eigen::Vector4f centroid(Eigen::Vector4f::Zero());
        pcl::compute3DCentroid(*rotated_item_in_hand_ptcld, centroid);
        transform.translation() = -centroid.head<3>();
        pcl::transformPointCloud(*rotated_item_in_hand_ptcld, *rotated_item_in_hand_ptcld, transform);
        // helpers::commons::visualize({rotated_item_in_hand_ptcld});
        transform = Eigen::Affine3f::Identity();
        Eigen::Matrix3f rotation(Eigen::AngleAxisf(rotation_angle, Eigen::Vector3f::UnitZ()));
        transform.rotate(rotation);
        pcl::transformPointCloud(*rotated_item_in_hand_ptcld, *rotated_item_in_hand_ptcld, transform);
        // helpers::commons::visualize({rotated_item_in_hand_ptcld});
        return rotated_item_in_hand_ptcld;
    }
    void Place::_getImageFromPtcld(const pcl::PointCloud<pcl::PointXYZ> &item_in_hand_ptcld, const float &occ_grid_leaf_size, cv::Mat &out_item_img)
    {
        out_item_img = cv::Mat::zeros(out_item_img.size(), out_item_img.type());
        for (auto const &pt : item_in_hand_ptcld.points)
        {
            size_t x_img = floor(pt.x / occ_grid_leaf_size);
            size_t y_img = floor((1 + pt.y) / occ_grid_leaf_size);
            out_item_img.at<uint8_t>(x_img, y_img) = OccGrid_e::OCCUPIED;
        }
    }
    std::tuple<int, int> Place::_getSearchLimits(const std::vector<Eigen::Vector3f> &target_area, const float &search_shift, const Eigen::Vector2f &obb_dims)
    {
        Eigen::Vector3f target_area_max(target_area[1]), target_area_min(target_area[0]);

        float height = abs(target_area_max.x() - target_area_min.x());
        float width = abs(target_area_max.y() - target_area_min.y());

        float shift_y = search_shift;
        float shift_x = search_shift;
        // in image
        int padded_height = floor((height - obb_dims.x()) / shift_x);
        int padded_width = floor((width - obb_dims.y()) / shift_y);
        return {padded_height, padded_width};
    }
} // namespace place
