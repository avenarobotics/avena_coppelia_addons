#include "plugin.h"

namespace place
{
    // // __________Coppelia plugin____________________
    PlacePlugin::PlacePlugin()
        : _parameters_read(false)
    {
    }
    PlacePlugin::~PlacePlugin()
    {
        if (_reading_parameters_thread.joinable())
            _reading_parameters_thread.join();
    };
    void PlacePlugin::onStart()
    {
        _node = rclcpp::Node::make_shared("place"); // TODO: Hasan Farag, remove node
        RCLCPP_INFO(_node->get_logger(), "Initialization of place server.");

        _parameters_read = false;

        _place_server = rclcpp_action::create_server<PlaceAction>(
            _node->get_node_base_interface(),
            _node->get_node_clock_interface(),
            _node->get_node_logging_interface(),
            _node->get_node_waitables_interface(),
            "place", // TODO: hasan Farag , replace with template
            std::bind(&PlacePlugin::_handle_goal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&PlacePlugin::_handle_cancel, this, std::placeholders::_1),
            std::bind(&PlacePlugin::_handle_accepted, this, std::placeholders::_1));

        auto qos_latching = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        _sub_manager = std::make_shared<helpers::SubscriptionsManager>(_node->get_node_topics_interface());

        _sub_manager->createSubscription(Topics::occupancy_grid.name, Topics::occupancy_grid.type);
        _sub_manager->createSubscription(Topics::items.name, Topics::items.type);

        _place_pub = _node->create_publisher<PlaceMsg>(Topics::place.name, qos_latching);

        // Run separate thread for reading parameters from the parameters server
        _reading_parameters_thread = std::thread([this]() {
            using namespace std::chrono_literals;
            while (rclcpp::ok())
            {
                std::this_thread::sleep_for(100ms);
                if (_getParams() == Status_e::SUCCESS)
                    break;
            }
        });

        RCLCPP_INFO(_node->get_logger(), "...Place server initialization is done");
    }
    void PlacePlugin::onInstancePass(const sim::InstancePassFlags &flags, bool first)
    {
        rclcpp::spin_some(_node);
    }

    //________Avena Data Validation___________

    int PlacePlugin::_validateMsg(const std::shared_ptr<const InputMsg> input_msg)
    {
        if (!_parameters_read)
        {
            RCLCPP_WARN(_node->get_logger(), "Areas data is not loaded yet");
            return Status_e::INVALID;
        }
        if (!input_msg)
        {
            RCLCPP_WARN(_node->get_logger(), "Invalid \"input\" message.");
            return Status_e::INVALID;
        }
        if (input_msg->occ_grid_msg.header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(_node->get_logger(), "Invalid header in \"occupancy_grid\" message.");
            return Status_e::INVALID;
        }
        if (input_msg->items_msg.header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(_node->get_logger(), "Invalid header in \"estimate_shape\" message.");
            return Status_e::INVALID;
        }
        return Status_e::SUCCESS;
    }
    int PlacePlugin::_validateGoal(const Goal &goal)
    {
        for (auto &area_label : goal.selected_areas_labels)
        {
            if (_params.table_areas_coords.find(area_label) != _params.table_areas_coords.end())
                continue;
            else
            {
                RCLCPP_WARN(_node->get_logger(), " Wrong area label '%s' ", area_label.c_str());
                return Status_e::INVALID;
            }
        }
        if (goal.search_shift < _MinSearchShift)
        {
            RCLCPP_WARN(_node->get_logger(), "wrong distance parameter passed, Goal Failed");
            return Status_e::INVALID;
        }
        if (goal.no_of_required_place_positions < _MinNoOfRequiredPlacePositions)
        {
            RCLCPP_WARN(_node->get_logger(), "Number of required place poses cant be lower than 1  :  %i ", goal.no_of_required_place_positions);
            RCLCPP_WARN(_node->get_logger(), "wrong  parameter passed, Goal Failed");
            return Status_e::INVALID;
        }
        if (goal.no_of_required_place_positions > _MaxNoOfRequiredPlacePositions)
        {
            RCLCPP_WARN(_node->get_logger(), "Number of required place poses cant be higher than 6  : %i ", goal.no_of_required_place_positions);
            RCLCPP_WARN(_node->get_logger(), "wrong  parameter passed, Goal Failed");
            return Status_e::INVALID;
        }
        return Status_e::SUCCESS;
    }
    int PlacePlugin::_validateOutput(const std::shared_ptr<const PlaceMsg> output_msg)
    {
        if (output_msg->place_poses.size() != 0)
        {
            return Status_e::SUCCESS;
        }
        else
        {
            RCLCPP_ERROR(_node->get_logger(), "Failed");
            return Status_e::FAILURE;
        }
    }

    void PlacePlugin::_terminate(const std::shared_ptr<GoalHandlePlaceAction> goal_handle)
    {
        _place_pub->publish(PlaceMsg());
        goal_handle->abort(std::make_shared<PlaceAction::Result>());
        RCLCPP_ERROR(_node->get_logger(), "Goal Failed");
    }
    //__________Place PLugin Main Fucntion_________
    void PlacePlugin::_execute(const std::shared_ptr<GoalHandlePlaceAction> goal_handle)
    {
        // helpers::Timer(__func__);
        RCLCPP_INFO(_node->get_logger(), "Place Executing goal");
        //-------------input data----------------
        auto input_msg = std::make_shared<place::InputMsg>();
        _getMsg(input_msg);
        if (_validateMsg(input_msg) != Status_e::SUCCESS)
        {
            _terminate(goal_handle);
            return;
        }
        //--------goal----------------------
        if (_validateGoal(_goal) != Status_e::SUCCESS)
        {
            _terminate(goal_handle);
            return;
        }
        // --------------------algorithm---------------------------------
        auto place = std::make_unique<Place>(_node->get_logger(), true);
        auto place_msg = std::make_shared<PlaceMsg>();
        if (place->execute(input_msg, _params, _goal, place_msg) != Status_e::SUCCESS)
        {
            _terminate(goal_handle);
            return;
        }
        //-----------Output Msg-----------------
        if (_validateOutput(place_msg) != Status_e::SUCCESS)
        {
            _terminate(goal_handle);
            return;
        }
        //----------------------publish---------------------
        _setMsg(input_msg, _goal, place_msg);
        _place_pub->publish(*place_msg);
        //-----------------------result----------------
        auto result = std::make_shared<PlaceAction::Result>();
        result->place_poses = place_msg->place_poses;
        goal_handle->succeed(result);
    }

    // ___________ data getters______________________
    void PlacePlugin::_getMsg(const std::shared_ptr<InputMsg> input_msg)
    {
        helpers::Timer(__func__);
        input_msg->items_msg = *(_sub_manager->getData<Items>(Topics::items.name));
        // input_msg->grasp_msg = *(_sub_manager->getData<Grasp>(Topics::grasp.name));
        input_msg->occ_grid_msg = *(_sub_manager->getData<OccGrid>(Topics::occupancy_grid.name));
    }
    // ___________ data setters______________________
    void PlacePlugin::_setMsg(const std::shared_ptr<const InputMsg> input_msg, const Goal &goal,
                              std::shared_ptr<PlaceMsg> output_msg)
    {
        helpers::Timer(__func__);
        output_msg->header.frame_id = "world";
        output_msg->header.stamp.sec = _node->now().seconds();
        output_msg->header.stamp.nanosec = _node->now().nanoseconds();
        //---passi->gg unecessay data for control team---//
        output_msg->grasp_poses = goal.grasp_msg.grasp_poses;
        output_msg->selected_item_id = goal.selected_item_id;
    }

    // __________ ROS Action Functions_______________
    rclcpp_action::GoalResponse PlacePlugin::_handle_goal(
        const rclcpp_action::GoalUUID &uuid,
        std::shared_ptr<const PlaceAction::Goal> goal)
    {
        _goal.grasp_msg.grasp_poses.resize(1);
        helpers::Timer(__func__);
        RCLCPP_INFO(_node->get_logger(), "Received goal ");
        try
        {
            _goal.search_shift = std::stof(goal->search_shift);
            _goal.selected_areas_labels = goal->selected_area_label;
            _goal.selected_area_operator = goal->selected_area_operator;
            _goal.no_of_required_place_positions = std::stoi(goal->place_positions_required);
            // RCLCPP_INFO(_node->get_logger(), "X: %f", goal->grasp_pose.grasp_pose.position.x);
            _goal.grasp_msg.grasp_poses[0] = goal->grasp_pose;
            // RCLCPP_INFO(_node->get_logger(), "ID: %i", goal->selected_item_id);
            _goal.selected_item_id = goal->selected_item_id;
        }
        catch (const json::exception &e)
        {
            RCLCPP_FATAL_STREAM(_node->get_logger(), "Exception with ID: " << e.id << "; message: " << e.what());
            return rclcpp_action::GoalResponse::REJECT;
        }
        RCLCPP_INFO(_node->get_logger(), "search_shift :  '%f'", _goal.search_shift);
        RCLCPP_INFO(_node->get_logger(), "selected_areas_labels : ");
        for (auto &area_label : _goal.selected_areas_labels)
        {
            RCLCPP_INFO(_node->get_logger(), "   '%s' ", area_label.c_str());
        }
        RCLCPP_INFO(_node->get_logger(), "selected_area_operator :  '%s'", _goal.selected_area_operator.c_str());
        RCLCPP_INFO(_node->get_logger(), "place_positions_required :  '%i'", _goal.no_of_required_place_positions);
        (void)uuid;
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse PlacePlugin::_handle_cancel(
        const std::shared_ptr<GoalHandlePlaceAction> goal_handle)
    {
        RCLCPP_INFO(_node->get_logger(), "Received request to cancel goal");
        (void)goal_handle;
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void PlacePlugin::_handle_accepted(const std::shared_ptr<GoalHandlePlaceAction> goal_handle)
    {
        _execute(goal_handle);
        // std::thread{std::bind(&PlacePlugin::_execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    //___________ ROS Params___________
    int PlacePlugin::_getParams()
    {
        helpers::Timer(__func__);
        RCLCPP_INFO_ONCE(_node->get_logger(), "Reading parameters from the server");
        // TODO: Hasan Farag - Read from param srvu
        _params.item_rotation_angle = M_PI / 6.0;

        if (_parameters_read)
            return Status_e::SUCCESS;

        using namespace std::chrono_literals;
        auto parameters_server_client = std::make_shared<rclcpp::AsyncParametersClient>(_node, "parameters_server");
        if (!parameters_server_client->wait_for_service(1s))
        {
            if (!rclcpp::ok())
            {
                RCLCPP_WARN(_node->get_logger(), "Interrupted while waiting for the service. Exiting...");
                return Status_e::FAILURE;
            }
            RCLCPP_WARN_ONCE(_node->get_logger(), "Parameters service not available. Trying to load parameters...");
            return Status_e::TIME_OUT;
        }
        RCLCPP_INFO(_node->get_logger(), "Parameters service available...");

        std::shared_future<std::vector<rclcpp::Parameter>> future_result = parameters_server_client->get_parameters({"areas_parameters"});
        if (future_result.wait_for(1s) == std::future_status::timeout)
        {
            RCLCPP_ERROR(_node->get_logger(), "Timeout when reading parameters...");
            return Status_e::TIME_OUT;
        }
        std::vector<rclcpp::Parameter> parameters = future_result.get();

        // Extract parameter values
        rclcpp::Parameter areas_parameter = parameters[0]; // we can safely access first element because we only request on parameter
        if (areas_parameter.get_type() != rclcpp::ParameterType::PARAMETER_STRING)
        {
            RCLCPP_ERROR(_node->get_logger(), "Invalid type of \"areas_parameters\" parameter...");
            return Status_e::INVALID;
        }
        std::string areas_str = areas_parameter.as_string();

        try
        {
            json area = json::parse(areas_str);
            auto &JsonToPosition = helpers::commons::assignPositionFromJson;

            // setting parameters from parameter server
            _params.occ_grid_leaf_size = std::stof(area["occ_grid_leaf_size"].dump());
            for (size_t area_id = 0; area_id < _TableAreasNames.size(); area_id++)
            {
                RCLCPP_INFO_STREAM(_node->get_logger(), "reading \"areas_parameters\": " << _TableAreasNames.at(area_id));
                if (area_id == Area_e::SECURITY_AREA)
                    _params.table_areas_coords[_TableAreasNames.at(area_id)] = {
                        {"min_exter", JsonToPosition(area[_TableAreasNames.at(area_id)]["min_exter"])},
                        {"max_exter", JsonToPosition(area[_TableAreasNames.at(area_id)]["max_exter"])},
                        {"min_inter", JsonToPosition(area[_TableAreasNames.at(area_id)]["min_inter"])},
                        {"max_inter", JsonToPosition(area[_TableAreasNames.at(area_id)]["max_inter"])}};
                else
                    _params.table_areas_coords[_TableAreasNames.at(area_id)] = {
                        {"min", JsonToPosition(area[_TableAreasNames.at(area_id)]["min"])},
                        {"max", JsonToPosition(area[_TableAreasNames.at(area_id)]["max"])}};
            }
            RCLCPP_INFO(_node->get_logger(), "Parameter server data loaded.");
        }
        catch (const json::exception &e)
        {
            RCLCPP_FATAL_STREAM(_node->get_logger(), "Exception with ID: " << e.id << "; message: " << e.what());
            return Status_e::INVALID;
        }
        _parameters_read = true;
        return Status_e::SUCCESS;
    }

} // namespace place

SIM_PLUGIN("PlacePlugin", 1, place::PlacePlugin)
#include "stubsPlusPlus.cpp"
