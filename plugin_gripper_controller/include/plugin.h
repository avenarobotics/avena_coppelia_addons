
#ifndef SIM_GRIPPER_CONTROLLER_H
#define SIM_GRIPPER_CONTROLLER_H

// ___COPPELIA PLUGINS___
#include "config.h"
#include "simPlusPlus/Plugin.h"
#include "stubs.h"

// ___CPP___
#include <memory>
#include <string>

//___ROS___
#include <rclcpp/rclcpp.hpp>
#include <rclcpp/time.hpp>
#include <custom_interfaces/msg/place.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <geometry_msgs/msg/point.hpp>

#include <rclcpp_action/rclcpp_action.hpp>
#include <custom_interfaces/action/simple_action.hpp>

#define PLUGIN_NAME "GripperController"
#define PLUGIN_VERSION 1.1

using namespace std::chrono_literals;

class GripperController : public sim::Plugin
{

public:
  GripperController();
  virtual ~GripperController(){};
  void onStart();
  void onInstancePass(const sim::InstancePassFlags &flags, bool first) override;
  void broadcastGripperState();

private:
  void _setGripperPositionByVelocity();
  void _getGripperConfigCallback(const custom_interfaces::msg::Place::SharedPtr gripper_config_msg);
  void _setGripperStatusCallback(const sensor_msgs::msg::JointState::SharedPtr gripper_set_status_msg);

  rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr _subscribe_gripper_status;

  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_gripper_state;


  sensor_msgs::msg::JointState::SharedPtr _gripper_set_status;

  rclcpp::Node::SharedPtr _node_controller;
  rclcpp::Node::SharedPtr _node_gripper;
  rclcpp::TimerBase::SharedPtr _timer;

};

#endif
