#ifndef SIM_GET_SIM_CAMERAS_DATA_PLUGIN_HPP
#define SIM_GET_SIM_CAMERAS_DATA_PLUGIN_HPP

// ___COPPELIA PLUGINS___
#include "config.h"
#include "simPlusPlus/Plugin.h"
#include "stubs.h"

// ___CPP___
#include <memory>

// ___Avena libraries___
#include <helpers.hpp>

//___ROS___
#include <rclcpp/rclcpp.hpp>
// #include <sensor_msgs/msg/point_cloud2.hpp>
// #include <geometry_msgs/msg/transform_stamped.hpp>
// #include <pcl_conversions/pcl_conversions.h>
// #include <tf2_ros/transform_listener.h>
// #include <tf2/LinearMath/Quaternion.h>
// #include <tf2_msgs/msg/tf_message.hpp>
#include "custom_interfaces/msg/cameras_data.hpp"

// ___Plugin___
#include "transforms_manager.hpp"

#define PLUGIN_NAME "GetSimCamerasData"
#define PLUGIN_VERSION 1

namespace get_sim_cameras_data
{
  using namespace helpers;
  using namespace std::chrono_literals;

  class GetSimCamerasData : public sim::Plugin, rclcpp::Node
  {
  public:
    GetSimCamerasData();
    virtual ~GetSimCamerasData();
    void onStart();
    void savePointcloud(savePointcloud_in *in, savePointcloud_out *out);
    void onInstancePass(const sim::InstancePassFlags &flags, bool first) override;

  private:
    pcl::PointCloud<pcl::PointXYZ>::Ptr _getPointcloudCameraFrame(std::string ptcld_buffer);
    pcl::PointCloud<pcl::PointXYZ>::Ptr _deserializePointCloud(const std::string &cloud_buffer);
    geometry_msgs::msg::Quaternion _coppeliaToGeometryQuat(std::vector<float> &quat);
    geometry_msgs::msg::Vector3 _coppeliaToGeometryPos(std::vector<float> &pos);
    void _getImages(std::map<std::string, cv::Mat> &out_images);
    void _fillMsgByImages(std::map<std::string, cv::Mat> &images, custom_interfaces::msg::CamerasData &out_cam_data);
    int _getTransforms(custom_interfaces::msg::CamerasData &out_cam_data);

    rclcpp::Publisher<custom_interfaces::msg::CamerasData>::SharedPtr _pub_cameras_data;
    TransformsManager::SharedPtr _transforms_manager;

    bool _debug;
  };
}

#endif // SIM_GET_SIM_CAMERAS_DATA_PLUGIN_HPP
