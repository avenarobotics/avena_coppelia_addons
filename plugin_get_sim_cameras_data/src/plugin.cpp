#include "plugin.h"
#include "simPlusPlus/Plugin.h"

namespace get_sim_cameras_data
{

    GetSimCamerasData::GetSimCamerasData()
        : Node("get_cameras_data"),
          _debug(false)
    {
    }

    void GetSimCamerasData::onStart()
    {
        if (!registerScriptStuff())
            throw std::runtime_error("script stuff initialization failed");

        setExtVersion("Plugin publishing simulation data (RGB, Depth, Point clouds, TF) data to ROS2 topic.");
        setBuildDate(BUILD_DATE);

        auto qos_latching = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local();
        _pub_cameras_data = create_publisher<custom_interfaces::msg::CamerasData>("cameras_data", qos_latching);
        _transforms_manager = std::make_shared<TransformsManager>(get_clock());
    }

    GetSimCamerasData::~GetSimCamerasData()
    {
    }

    void GetSimCamerasData::onInstancePass(const sim::InstancePassFlags &flags, bool first)
    {
        rclcpp::spin_some(get_node_base_interface());
    }

    void GetSimCamerasData::savePointcloud(savePointcloud_in *in, savePointcloud_out *out)
    {
        Timer timer("GetCamerasData::savePointcloud", _debug);
        // RCLCPP_INFO(this->get_logger(), "savePointcloud");
        custom_interfaces::msg::CamerasData cam_data;
        if (_getTransforms(cam_data))
            return;

        pcl::PointCloud<pcl::PointXYZ>::Ptr camera1_cloud = _getPointcloudCameraFrame(in->ptcld_buffer_cam1);
        pcl::PointCloud<pcl::PointXYZ>::Ptr camera2_cloud = _getPointcloudCameraFrame(in->ptcld_buffer_cam2);
        helpers::converters::pclToRosPtcld<pcl::PointXYZ>(camera1_cloud, cam_data.cam1_ptcld);
        helpers::converters::pclToRosPtcld<pcl::PointXYZ>(camera2_cloud, cam_data.cam2_ptcld);
        cam_data.cam1_ptcld.header.frame_id = "camera1";
        cam_data.cam2_ptcld.header.frame_id = "camera2";
        cam_data.cam1_ptcld.header.stamp = now();
        cam_data.cam2_ptcld.header.stamp = now();

        std::map<std::string, cv::Mat> images;
        _getImages(images);
        _fillMsgByImages(images, cam_data);
        // std::cout << cam_data.cam1_rgb.width << ","
        //           << cam_data.cam2_rgb.width << ","
        //           << cam_data.cam1_depth.width << ","
        //           << cam_data.cam2_depth.width << std::endl;
        // std::cout << cam_data.cam1_ptcld.width << "," << cam_data.cam2_ptcld.width << std::endl;

        cam_data.header.frame_id = "cameras";
        cam_data.header.stamp = this->now();
        _pub_cameras_data->publish(cam_data);
    }

    void GetSimCamerasData::_getImages(std::map<std::string, cv::Mat> &out_images)
    {
        Timer timer("GetCamerasData::_getImages", _debug);

        //THOSE VALUES CHANGE EACH TIME U CHANGE ANY PARAMETER OF CAMERA!
        // cv::Rect2i roi(216, 290, 807 - 216, 733 - 290);
        // cv::Rect2i roi(216, 345,591, 334);
        size_t rgb1 = simGetObjectHandle("RGB1");
        size_t rgb2 = simGetObjectHandle("RGB2");
        size_t depth1 = simGetObjectHandle("Depth1");
        size_t depth2 = simGetObjectHandle("Depth2");

        simInt resolution[2];
        simGetVisionSensorResolution(rgb1, &resolution[0]);
        // auto handling_start = std::chrono::system_clock::now();
        simHandleVisionSensor(sim_handle_all, nullptr, nullptr);
        // auto handling_end = std::chrono::system_clock::now();
        // std::cout << "handling durarion " << std::chrono::duration_cast<std::chrono::milliseconds>(handling_end - handling_start).count() << std::endl;

        //camera 1
        //rgb
        simUChar *data_cam1 = simGetVisionSensorCharImage(rgb1, &resolution[0], &resolution[1]);
        // std::cout << "resolutions are "<< resolution[0] << "," << resolution[1] << std::endl;
        if (!data_cam1)
        {
            // _connector->log(log_level::error, getenv("_"), "Cannot get data from camera rgb no.1");
            std::cout << "Cannot get data from camera rgb no.1" << std::endl;
            throw std::runtime_error(std::string("Cannot get data from camera rgb no.1"));
        }
        cv::Mat cam1_rgb(resolution[1], resolution[0], CV_8UC3);
        memcpy(cam1_rgb.data, data_cam1, resolution[0] * resolution[1] * 3);
        cv::cvtColor(cam1_rgb, cam1_rgb, cv::COLOR_RGB2BGR);
        simReleaseBuffer(reinterpret_cast<simChar *>(data_cam1));
        cv::flip(cam1_rgb, cam1_rgb, 1);
        cv::Mat cropped1 = cam1_rgb(cv::Rect(0, 0, 1280, 720));
        out_images.insert(std::make_pair("camera_1_rgb", cropped1));

        //depth
        simGetVisionSensorResolution(depth1, &resolution[0]);
        simFloat *depth_data_cam1 = simGetVisionSensorDepthBuffer(depth1 + sim_handleflag_depthbuffermeters);
        if (!depth_data_cam1)
        {
            // _connector->log(log_level::error, getenv("_"), "Cannot get data from camera depth no.1");
            std::cout << "Cannot get data from camera depth no.1" << std::endl;
            throw std::runtime_error(std::string("Cannot get data from camera depth no.1"));
        }
        cv::Mat depth_cam1(resolution[1], resolution[0], CV_32FC1);
        memcpy(depth_cam1.data, depth_data_cam1, resolution[0] * resolution[1] * sizeof(float));
        simReleaseBuffer(reinterpret_cast<simChar *>(depth_data_cam1));
        cv::flip(depth_cam1, depth_cam1, 1);
        // cv::Mat depth_cam1_roi = depth_cam1(roi).clone();
        // out_images.insert(std::make_pair("camera_1_depth", depth_cam1_roi));
        out_images.insert(std::make_pair("camera_1_depth", depth_cam1));

        //camera 2
        //rgb
        simGetVisionSensorResolution(rgb2, &resolution[0]);
        simUChar *data_cam2 = simGetVisionSensorCharImage(rgb2, &resolution[0], &resolution[1]);
        if (!data_cam2)
        {
            // _connector->log(log_level::error, getenv("_"), "Cannot get data from camera rgb no.2");
            std::cout << "Cannot get data from camera depth rgb no.2" << std::endl;
            throw std::runtime_error(std::string("Cannot get data from camera rgb no.2"));
        }
        cv::Mat cam2_rgb(resolution[1], resolution[0], CV_8UC3);
        memcpy(cam2_rgb.data, data_cam2, resolution[0] * resolution[1] * 3);
        cv::cvtColor(cam2_rgb, cam2_rgb, cv::COLOR_RGB2BGR);
        simReleaseBuffer(reinterpret_cast<simChar *>(data_cam2));
        cv::flip(cam2_rgb, cam2_rgb, 1);

        cv::Mat cropped2 = cam2_rgb(cv::Rect(0, 0, 1280, 720));
        out_images.insert(std::make_pair("camera_2_rgb", cropped2));

        //depth
        simGetVisionSensorResolution(depth2, &resolution[0]);
        simFloat *depth_data_cam2 = simGetVisionSensorDepthBuffer(depth2 + sim_handleflag_depthbuffermeters);
        if (!depth_data_cam2)
        {
            // _connector->log(log_level::error, getenv("_"), "Cannot get data from camera depth no.2");
            std::cout << "Cannot get data from camera depth no.2" << std::endl;
            throw std::runtime_error(std::string("Cannot get data from camera depth no.2"));
        }
        cv::Mat depth_cam2(resolution[1], resolution[0], CV_32FC1);
        memcpy(depth_cam2.data, depth_data_cam2, resolution[0] * resolution[1] * sizeof(float));
        simReleaseBuffer(reinterpret_cast<simChar *>(depth_data_cam2));
        cv::flip(depth_cam2, depth_cam2, 1);
        // cv::Mat depth_cam2_roi = depth_cam2(roi).clone();
        // out_images.insert(std::make_pair("camera_2_depth", depth_cam2_roi));
        out_images.insert(std::make_pair("camera_2_depth", depth_cam2));
    }

    void GetSimCamerasData::_fillMsgByImages(std::map<std::string, cv::Mat> &images, custom_interfaces::msg::CamerasData &out_cam_data)
    {
        Timer timer("GetCamerasData::_fillMsgByImages", _debug);

        helpers::converters::cvMatToRos(images["camera_1_rgb"], out_cam_data.cam1_rgb);
        helpers::converters::cvMatToRos(images["camera_2_rgb"], out_cam_data.cam2_rgb);
        helpers::converters::cvMatToRos(images["camera_1_depth"], out_cam_data.cam1_depth);
        helpers::converters::cvMatToRos(images["camera_2_depth"], out_cam_data.cam2_depth);

        auto fill_header = [this](sensor_msgs::msg::Image &image, std::string camera_frame) {
            image.header.frame_id = camera_frame;
            image.header.stamp = now();
        };
        fill_header(out_cam_data.cam1_rgb, "camera1");
        fill_header(out_cam_data.cam1_depth, "camera1");
        fill_header(out_cam_data.cam2_rgb, "camera2");
        fill_header(out_cam_data.cam2_depth, "camera2");
    }

    int GetSimCamerasData::_getTransforms(custom_interfaces::msg::CamerasData &out_cam_data)
    {
        Timer timer("GetCamerasData::_getTransforms", _debug);
        out_cam_data.robot_transforms.clear();
        auto update_robot_transforms = [this](std::vector<geometry_msgs::msg::TransformStamped> &robot_transforms, const std::string &parent_frame, const std::string &child_frame) -> int {
            auto transform = _transforms_manager->lookupLatestTransform(parent_frame, child_frame);
            if (!transform)
                return 1;
            robot_transforms.push_back(*transform);
            return 0;
        };
        const size_t nr_arm_frames = 8;
        std::string parent_frame = "world";
        std::string child_frame = "panda_link0";

        // Arm TF
        for (size_t frame_id = 0; frame_id < nr_arm_frames; ++frame_id)
        {
            if (update_robot_transforms(out_cam_data.robot_transforms, parent_frame, child_frame))
                return 1;
            parent_frame = child_frame;
            child_frame = "panda_link" + std::to_string(frame_id + 1);
        }

        // Gripper TF
        if (update_robot_transforms(out_cam_data.robot_transforms, "panda_link7", "Panda_gripper"))
            return 1;

        if (update_robot_transforms(out_cam_data.robot_transforms, "Panda_gripper", "panda_hand"))
            return 1;

        if (update_robot_transforms(out_cam_data.robot_transforms, "panda_hand", "panda_leftfinger"))
            return 1;

        if (update_robot_transforms(out_cam_data.robot_transforms, "panda_hand", "panda_rightfinger"))
            return 1;

        RCLCPP_INFO_ONCE(get_logger(), "Transforms read from /tf topic.");
        return 0;
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr GetSimCamerasData::_getPointcloudCameraFrame(std::string ptcld_buffer)
    {
        Timer timer("GetCamerasData::_getPointcloudCameraFrame", _debug);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = _deserializePointCloud(ptcld_buffer);
        commons::passThroughFilter(cloud, "z", 0.1, 10);
        return cloud;
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr GetSimCamerasData::_deserializePointCloud(const std::string &cloud_buffer)
    {
        // Timer timer("_deserializePointCloud", _debug);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
        cloud->points.resize((cloud_buffer.size() / sizeof(float) - 2) * 3 / 4);
        for (size_t i = 8; i < cloud_buffer.size(); i += 16)
        {
            std::memcpy(&(cloud->points[i / 16].x), &(cloud_buffer[i]), sizeof(float));
            std::memcpy(&(cloud->points[i / 16].y), &(cloud_buffer[i + 4]), sizeof(float));
            std::memcpy(&(cloud->points[i / 16].z), &(cloud_buffer[i + 8]), sizeof(float));
        }
        return cloud;
    }

    geometry_msgs::msg::Quaternion GetSimCamerasData::_coppeliaToGeometryQuat(std::vector<float> &quat)
    {
        geometry_msgs::msg::Quaternion rot;
        rot.x = quat[0];
        rot.y = quat[1];
        rot.z = quat[2];
        rot.w = quat[3];
        return rot;
    }

    geometry_msgs::msg::Vector3 GetSimCamerasData::_coppeliaToGeometryPos(std::vector<float> &pos)
    {
        geometry_msgs::msg::Vector3 position;
        position.x = pos[0];
        position.y = pos[1];
        position.z = pos[2];
        return position;
    }
} // namespace get_sim_cameras_data

SIM_PLUGIN(PLUGIN_NAME, PLUGIN_VERSION, get_sim_cameras_data::GetSimCamerasData)
#include "stubsPlusPlus.cpp"
