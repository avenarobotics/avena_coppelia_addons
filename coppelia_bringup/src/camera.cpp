#include "rclcpp/rclcpp.hpp"

#include <iostream> // stringstream

#include <cstdio>

#define COPPELIA_EXECUTABLE "/opt/avena/coppelia_camera/coppeliaSim.sh"
#define COPPELIA_SCENE "/opt/avena/scenes/scenes/ava01_scene_FrankaArm.ttt"

int main(int argc, char **argv)
{
    // node name: webots
    rclcpp::init(argc, argv);
    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("coppelia_camera");

    RCLCPP_INFO(node->get_logger(), "Starting Coppelia simulator... \n");
    RCLCPP_INFO(node->get_logger(), "Loading scene: %s \n", COPPELIA_SCENE);
    if (system(COPPELIA_EXECUTABLE " " COPPELIA_SCENE)) {}

    return 0;
}
