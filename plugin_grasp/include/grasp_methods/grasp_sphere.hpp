#ifndef GRASP_SPHERE_HPP
#define GRASP_SPHERE_HPP

#include "grasp_methods/base_grasp_method.hpp"
#include "visualization.hpp"
#include <rclcpp/rclcpp.hpp>

class GraspSphere : public BaseGraspMethod
{

public:
    // GraspSphere();
    // virtual ~GraspSphere();
    virtual int genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses) override;
    int genereateGrasp(Item item_data, std::vector<Eigen::Affine3f> &grasp_poses);

private:
    std::vector<Eigen::Affine3f> _findSphereGraspOrientation(Item item_data);
    Eigen::Affine3f _computeGraspPose(Eigen::Affine3f obj_pose, Eigen::Vector3f approach_angles, Eigen::Vector3f center_displacement);
    void _alignGraspOrientation(Eigen::Affine3f obj_pose, Eigen::Affine3f &out_pregrasp_pose, Eigen::Vector3f approach_angles);
    void _computeGraspPosition(Eigen::Affine3f obj_pose, Eigen::Vector3f center_displacement_vec, Eigen::Affine3f &out_grasp_pose);
    bool _readItemData(custom_interfaces::msg::Item &item, Item &item_data);
};

#endif
