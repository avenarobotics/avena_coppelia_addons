#ifndef GRASP_BOX_HPP
#define GRASP_BOX_HPP

#include "grasp_methods/base_grasp_method.hpp"

class GraspBox : public BaseGraspMethod
{

public:
    // GraspBox();
    // virtual ~GraspBox();
    virtual int genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses) override;

private:
std::vector<Eigen::Affine3f> _findBoxGraspPose(Item &item_data);
bool _readItemData(custom_interfaces::msg::Item &item, Item &item_data);




};

#endif
