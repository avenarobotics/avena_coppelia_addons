#ifndef GRASP_ONION_HPP
#define GRASP_ONION_HPP

#include "grasp_methods/base_grasp_method.hpp"
#include "grasp_methods/grasp_sphere.hpp"

class GraspOnion : public BaseGraspMethod
{

public:
    virtual int genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses) override;

private:
    Item _readItemData(custom_interfaces::msg::Item &item);
};

#endif
