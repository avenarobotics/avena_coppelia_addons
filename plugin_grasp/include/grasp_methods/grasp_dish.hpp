#ifndef GRASP_DISH_HPP
#define GRASP_DISH_HPP

#include "grasp_methods/base_grasp_method.hpp"

class GraspDish : public BaseGraspMethod
{

public:
    // GraspSphere();
    // virtual ~GraspSphere();
    virtual int genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses) override;

private:
    bool _readItemData(custom_interfaces::msg::Item &item, Item &item_data);
    bool _computeInitialGrasp(Item &item_data, Eigen::Affine3f &initial_grasp);
    bool _computeGrasps(Item &item_data, Eigen::Affine3f &initial_grasp, std::vector<Eigen::Affine3f> &out_grasps);
    bool _printWarn(std::string warn_msg);

    float _grasp_offset = 0.02;
};

#endif
