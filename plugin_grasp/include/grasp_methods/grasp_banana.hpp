#ifndef GRASP_BANANA_HPP
#define GRASP_BANANA_HPP

#include "grasp_methods/base_grasp_method.hpp"
#include "grasp_methods/grasp_cylinder.hpp"

class GraspBanana : public BaseGraspMethod
{

public:
    // GraspSphere();
    // virtual ~GraspSphere();
    virtual int genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses) override;

private:
    bool _readItemData(custom_interfaces::msg::Item &item, Item &banana_handle_data);
};

#endif
