#include "grasp_methods/grasp_box.hpp"

bool GraspBox::_readItemData(custom_interfaces::msg::Item &item, Item &item_data)
{

    if (item.item_elements[0].primitive_shape != "")
    {

        json primitive_shape = json::parse(item.item_elements[0].primitive_shape);
        if (!primitive_shape.contains("dims") ||
            !primitive_shape["dims"].contains("x") ||
            !primitive_shape["dims"].contains("y") ||
            !primitive_shape["dims"].contains("z"))
        {
            RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about object dimensions");
            return false;
        }
        item_data.obj_dim.resize(3);
        item_data.obj_dim[0] = primitive_shape["dims"]["x"];
        item_data.obj_dim[1] = primitive_shape["dims"]["y"];
        item_data.obj_dim[2] = primitive_shape["dims"]["z"];
    }
    else
    {
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about object dimensions");
        return false;
    }

    if (item.item_elements[0].position != "" && item.item_elements[0].orientation != "")
    {
        Eigen::Translation3f trans;

        json position = json::parse(item.item_elements[0].position);
        if (!position.contains("x") || !position.contains("y") || !position.contains("z"))
        {
            RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about position");
            return false;
        }

        trans.x() = position["x"];
        trans.y() = position["y"];
        trans.z() = position["z"];
        json orientation = json::parse(item.item_elements[0].orientation);
        if (!orientation.contains("x") || !orientation.contains("y") || !orientation.contains("z") || !orientation.contains("w"))
        {
            RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about orientation");
            return false;
        }
        Eigen::Quaternionf quat;
        quat.w() = orientation["w"];
        quat.x() = orientation["x"];
        quat.y() = orientation["y"];
        quat.z() = orientation["z"];
        item_data.obj_pose = trans * quat;
    }
    else
    {
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about object pose");
        return false;
    }

    return true;
}

int GraspBox::genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses)
{
    Item item_data;
     if(!_readItemData(item, item_data))
        return 1;

    if (item_data.obj_dim.size() == 0)
        return 1;

    grasp_poses = _findBoxGraspPose(item_data);
    _filterGrasps(grasp_poses);
    _sortGrasps(grasp_poses, item_data);

    return 0;
}

std::vector<Eigen::Affine3f> GraspBox::_findBoxGraspPose(Item &item_data)
{
    std::vector<Eigen::Affine3f> grasp_poses;
    Eigen::Quaternionf orient(item_data.obj_pose.rotation());
    Eigen::Vector3f pos = item_data.obj_pose.translation();
    orient *= Eigen::Quaternionf(helpers::commons::assignRotationMatrixAroundX(M_PI_2));
    orient *= Eigen::Quaternionf(helpers::commons::assignRotationMatrixAroundY(M_PI_2));

    Eigen::Affine3f initial_grasp = Eigen::Translation3f(pos) * orient;
    float full_hand_length = (_gripper_params.hand_length + _gripper_params.finger_length);
    for (int i = 0; i < 4; i++)
    {
        Eigen::Quaternionf pose_orient(initial_grasp.rotation());
        pose_orient = pose_orient * helpers::commons::assignRotationMatrixAroundZ(M_PI_2 * i);
        float offset;
        if (i % 2 == 1)
            offset = std::max((item_data.obj_dim[2] / 2 - _gripper_params.finger_length) + full_hand_length, full_hand_length);
        else
            offset = std::max((item_data.obj_dim[0] / 2 - _gripper_params.finger_length) + full_hand_length, full_hand_length);

        Eigen::Vector3f pose_position = initial_grasp.translation() - (pose_orient.toRotationMatrix().col(0) * offset);
        Eigen::Affine3f new_pose = Eigen::Translation3f(pose_position) * pose_orient;
        grasp_poses.push_back(new_pose);
    }

    for (int i = 0; i < 2; i++)
    {
        Eigen::Quaternionf pose_orient(initial_grasp.rotation());
        pose_orient = pose_orient * helpers::commons::assignRotationMatrixAroundY(M_PI_2 + M_PI * i);
        pose_orient = pose_orient * helpers::commons::assignRotationMatrixAroundX(M_PI_2);
        float offset = std::max((item_data.obj_dim[1] / 2 - _gripper_params.finger_length) + full_hand_length, full_hand_length);
        Eigen::Vector3f pose_position = initial_grasp.translation() - (pose_orient.toRotationMatrix().col(0) * offset);
        Eigen::Affine3f new_pose = Eigen::Translation3f(pose_position) * pose_orient;
        grasp_poses.push_back(new_pose);
    }

    int grasps_size = grasp_poses.size();
    for (int i = 0; i < grasps_size; i++)
    {
        for (int j = 1; j < 4; j++)
        {
            Eigen::Affine3f new_grasp;
            Eigen::Quaternionf new_orient(grasp_poses[i].rotation());
            new_orient = new_orient * helpers::commons::assignRotationMatrixAroundX(M_PI_2 * j);
            new_grasp = Eigen::Translation3f(grasp_poses[i].translation()) * new_orient;
            grasp_poses.push_back(new_grasp);
        }
    }

    grasps_size = grasp_poses.size();

    for (int i = 0; i < grasps_size; i++)
    {
        Eigen::Vector3f initial_position = initial_grasp.translation();
        for (int j = 1; j <= 6; j++)
        {
            for (int k = -1; k <= 1; k += 2)
            {
                float grasp_offset = (grasp_poses[i].translation() - initial_position).norm();
                float angle = k * (M_PI / 36) * j;
                Eigen::Quaternionf rotation_y(helpers::commons::assignRotationMatrixAroundY(angle));
                // grasp_offset = grasp_offset / cos(abs(angle));
                grasp_offset = cos(abs(angle)) * grasp_offset;
                // cos *grasp_offset= a/

                Eigen::Quaternionf new_grasp_rotation = Eigen::Quaternionf(grasp_poses[i].rotation()) * rotation_y;
                Eigen::Vector3f new_grasp_position = initial_position - (new_grasp_rotation.toRotationMatrix().col(0) * grasp_offset);
                Eigen::Affine3f new_grasp = Eigen::Translation3f(new_grasp_position) * new_grasp_rotation;
                grasp_poses.push_back(new_grasp);
            }
        }
    }

    return grasp_poses;
}