#include "grasp_methods/base_grasp_method.hpp"

// BaseGraspMethod::BaseGraspMethod(){};

std::pair<pcl::PointXYZ, pcl::PointXYZ> BaseGraspMethod::loadGripperProperties(GripperParams &gripper_params)
{
    _gripper_params = gripper_params;
    float gripper_finger_offset = 0.005;
    _gripper_params.finger_length -= gripper_finger_offset;
    pcl::PointCloud<pcl::PointXYZ>::Ptr empty_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    _cloud = empty_cloud;

    _hand_boundry.first.x = _gripper_params.hand_length;
    _hand_boundry.first.y = -(_gripper_params.max_grasp_width/2) - _gripper_params.finger_depth;
    _hand_boundry.first.z = -_gripper_params.hand_depth/2;
    
    _hand_boundry.second.x = _gripper_params.hand_length + _gripper_params.finger_length;
    _hand_boundry.second.y = _gripper_params.max_grasp_width/2 + _gripper_params.finger_depth;
    _hand_boundry.second.z = _gripper_params.hand_depth/2;


    _grip_area = _hand_boundry;
    _grip_area.first.y += _gripper_params.finger_depth;
    _grip_area.second.y -= _gripper_params.finger_depth;

    return _grip_area;
}

int BaseGraspMethod::setScenePtcld(pcl::PointCloud<pcl::PointXYZ>::Ptr scene_cloud)
{
    *_cloud = *scene_cloud;
    return 0;
};

int BaseGraspMethod::_filterGrasps(std::vector<Eigen::Affine3f> &grasp_candidates)
{
    for (auto it = grasp_candidates.begin(); it != grasp_candidates.end();)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr transformed(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::transformPointCloud(*_cloud, *transformed, it->inverse());
        if (it->translation().z() < _gripper_params.hand_depth)
            it = grasp_candidates.erase(it);
        else if (!_isGraspValid(*it))
            it = grasp_candidates.erase(it);
        else
            it++;
    }
    return 0;
}

int BaseGraspMethod::_sortGrasps(std::vector<Eigen::Affine3f> &grasp_candidates, Item &item_data)
{
    Eigen::Vector3f object_position = item_data.obj_pose.translation();
    std::sort(grasp_candidates.begin(), grasp_candidates.end(), [object_position](Eigen::Affine3f &a, Eigen::Affine3f &b) {
        Eigen::Vector3f a_vec = a.rotation().col(0);
        Eigen::Vector3f b_vec = b.rotation().col(0);
        float angle_a = acos(a_vec.dot(-Eigen::Vector3f::UnitZ()));
        float angle_b = acos(b_vec.dot(-Eigen::Vector3f::UnitZ()));
        if (std::isnan(angle_a))
            angle_a = 0;
        if (std::isnan(angle_b))
            angle_b = 0;

        a_vec = a.translation();
        b_vec = b.translation();
        float distance_a = (a_vec - object_position).norm();
        float distance_b = (b_vec - object_position).norm();

        if (angle_a == angle_b)
            return (distance_a < distance_b);
        else
            return (angle_a < angle_b);
    });
    return 0;
}

bool BaseGraspMethod::_isGraspValid(Eigen::Affine3f grasp_pose)
{
    std::vector<int> indices;
    pcl::CropBox<pcl::PointXYZ> crop_box;
    size_t points_ammount;
    crop_box.setInputCloud(_cloud);
    crop_box.setMin(_hand_boundry.first.getVector4fMap());
    crop_box.setMax(_hand_boundry.second.getVector4fMap());
    crop_box.setTransform(grasp_pose.inverse());
    crop_box.filter(indices);
    if (indices.size() == 0)
        return false;
    points_ammount = indices.size();
    indices.clear();
    crop_box.setMin(_grip_area.first.getVector4fMap());
    crop_box.setMax(_grip_area.second.getVector4fMap());
    crop_box.setTransform(grasp_pose.inverse());

    crop_box.filter(indices);
    return (points_ammount == indices.size());
}