#include "grasp_methods/grasp_broccoli.hpp"

Item GraspBroccoli::_readItemData(custom_interfaces::msg::Item &item)
{
    Item broccoli_handle_data;

    for (auto el : item.item_elements)
    {
        json primitive_shape = json::parse(el.primitive_shape);
        if (primitive_shape.contains("fit_method") && primitive_shape["fit_method"] == "cylinder")
        {
            json pos = json::parse(el.position);
            Eigen::Vector3f position = helpers::commons::assignPositionFromJson(pos);
            json orient = json::parse(el.orientation);
            Eigen::Quaternionf rotation = helpers::commons::assignQuaternionFromJson(orient);
            broccoli_handle_data.obj_pose = Eigen::Translation3f(position) * rotation;
            broccoli_handle_data.obj_dim.resize(2);
            broccoli_handle_data.obj_dim[0] = primitive_shape["dims"]["height"];
            broccoli_handle_data.obj_dim[1] = primitive_shape["dims"]["radius"];
        }
    }

    return broccoli_handle_data;
}

int GraspBroccoli::genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses)
{
    Item item_data = _readItemData(item);

    if (item_data.obj_dim.size() != 2)
        return 1;
    GraspCylinder grasp_cylinder;
    grasp_cylinder.loadGripperProperties(_gripper_params);
    grasp_cylinder.setScenePtcld(_cloud);
    grasp_cylinder.genereateGrasp(item_data, grasp_poses);

    return 0;
}
