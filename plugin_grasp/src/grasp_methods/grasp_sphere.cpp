#include "grasp_methods/grasp_sphere.hpp"

// GraspSphere::GraspSphere()
// {
//     // loadGripperProperties();

// };

bool GraspSphere::_readItemData(custom_interfaces::msg::Item &item, Item &item_data)
{
    if (item.item_elements[0].primitive_shape != "")
    {
        json primitive_shape = json::parse(item.item_elements[0].primitive_shape);

        if (!primitive_shape.contains("dims") || !primitive_shape["dims"].contains("radius")){
            RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about object dimensions");
            return false;
        }

        item_data.obj_dim.resize(1);
        item_data.obj_dim[0] = primitive_shape["dims"]["radius"];
    }
    else{
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about object dimensions");
        return false;

    }

    if (item.item_elements[0].position != "" && item.item_elements[0].orientation != "")
    {
        Eigen::Translation3f trans;
        json position = json::parse(item.item_elements[0].position);

        if(!position.contains("x") || !position.contains("y") || !position.contains("z")){
            RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about position");
            return false;
        }

        trans.x() = position["x"];
        trans.y() = position["y"];
        trans.z() = position["z"];
        json orientation = json::parse(item.item_elements[0].orientation);
        if(!orientation.contains("x") || !orientation.contains("y") || !orientation.contains("z") || !orientation.contains("w")){
            RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about orientation");
            return false;
        }

        Eigen::Quaternionf quat;
        quat.w() = orientation["w"];
        quat.x() = orientation["x"];
        quat.y() = orientation["y"];
        quat.z() = orientation["z"];
        item_data.obj_pose = trans * quat;
    }
    else{
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about object pose");
        return false;
    }

    return true;
}

int GraspSphere::genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses)
{
    Item item_data;

    if(!_readItemData(item, item_data))
        return 1;


    if (item_data.obj_dim[0] > _gripper_params.finger_length)
    {
        RCLCPP_INFO(rclcpp::get_logger("grasp"), "estimated sphere diameter = " + std::to_string(item_data.obj_dim[0]) );
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "sphere diameter to big to grasp.");
        return 1;
    }
    else if (item_data.obj_dim.size() == 0)
        return 1;

    grasp_poses = _findSphereGraspOrientation(item_data);
    _filterGrasps(grasp_poses);
    _sortGrasps(grasp_poses, item_data);

    return 0;
}

int GraspSphere::genereateGrasp(Item item_data, std::vector<Eigen::Affine3f> &grasp_poses)
{
    if (item_data.obj_dim[0] > _gripper_params.finger_length)
    {
        RCLCPP_INFO(rclcpp::get_logger("grasp"), "estimated sphere diameter = " + std::to_string(item_data.obj_dim[0]) );
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "sphere diameter to big to grasp.");
        return 1;
    }
    else if (item_data.obj_dim.size() == 0)
        return 1;

    grasp_poses = _findSphereGraspOrientation(item_data);
    _filterGrasps(grasp_poses);
    _sortGrasps(grasp_poses, item_data);
    return 0;
}

std::vector<Eigen::Affine3f> GraspSphere::_findSphereGraspOrientation(Item item_data)
{
    Eigen::Vector2f roll_pitch_diff(M_PI / 9, M_PI / 18);
    Eigen::Vector3f approach_angles(0, 0, 0);
    std::vector<Eigen::Affine3f> grasp_poses;
    size_t grasps_ammount = (static_cast<int>(floor(M_PI / roll_pitch_diff[1])) + 1) * (static_cast<int>(floor(M_PI / roll_pitch_diff[0])) + 1);
    grasp_poses.reserve(grasps_ammount);
    for (int i = 0; i <= static_cast<int>(floor(M_PI / roll_pitch_diff[1])); i++)
    {

        if (i % 2 == 1)
            approach_angles[1] = roll_pitch_diff[1] * (i + 1) / 2;
        else
            approach_angles[1] *= -1;

        approach_angles[0] = 0.0;
        for (int j = 0; j <= static_cast<int>(floor(M_PI / roll_pitch_diff[0])); j++)
        {
            approach_angles[0] = roll_pitch_diff[0] * j;
            grasp_poses.push_back(_computeGraspPose(item_data.obj_pose, approach_angles, {0, 0, 0}));
        }
    }
    grasp_poses.shrink_to_fit();
    return grasp_poses;
}

Eigen::Affine3f GraspSphere::_computeGraspPose(Eigen::Affine3f obj_pose, Eigen::Vector3f approach_angles, Eigen::Vector3f center_displacement)
{
    Eigen::Affine3f grasp_pose;
    _alignGraspOrientation(obj_pose, grasp_pose, approach_angles);
    _computeGraspPosition(obj_pose, center_displacement, grasp_pose);
    return grasp_pose;
}

void GraspSphere::_alignGraspOrientation(Eigen::Affine3f obj_pose, Eigen::Affine3f &out_pregrasp_pose, Eigen::Vector3f approach_angles)
{
    Eigen::Quaternionf orientation;

    Eigen::Vector3f sphere_pose_vec = obj_pose.translation();
    Eigen::Quaternionf rotation(1, 0, 0, 0);
    Eigen::Quaternionf x_angle;
    sphere_pose_vec[2] = 0.0;
    x_angle.setFromTwoVectors(Eigen::Vector3f::UnitX(), sphere_pose_vec);
    orientation = x_angle * helpers::commons::assignRotationMatrixAroundY(-M_PI / 2);
    orientation = orientation * helpers::commons::assignRotationMatrixAroundZ(M_PI / 2);

    orientation = orientation * helpers::commons::assignRotationMatrixAroundZ(-M_PI / 2);
    Eigen::Vector3f x_axis = orientation.toRotationMatrix().col(0);
    if (x_axis[2] > 0)
    {
        orientation = orientation * helpers::commons::assignRotationMatrixAroundZ(M_PI);
    }
    orientation = orientation * helpers::commons::assignRotationMatrixAroundX(approach_angles[0]);
    orientation = orientation * helpers::commons::assignRotationMatrixAroundY(approach_angles[1]);
    orientation = orientation * helpers::commons::assignRotationMatrixAroundZ(approach_angles[2]);
    out_pregrasp_pose = Eigen::Translation3f(0, 0, 0) * orientation;
}

void GraspSphere::_computeGraspPosition(Eigen::Affine3f obj_pose, Eigen::Vector3f center_displacement_vec, Eigen::Affine3f &out_grasp_pose)
{
    Eigen::Quaternionf orientation(out_grasp_pose.rotation());

    Eigen::Vector3f x_axis = orientation.toRotationMatrix().col(0);
    Eigen::Vector3f z_axis = orientation.toRotationMatrix().col(2);
    Eigen::Vector3f gripper_position(obj_pose.translation().x(), obj_pose.translation().y(), obj_pose.translation().z());
    gripper_position -= (x_axis *  (_gripper_params.hand_length+_gripper_params.finger_length));
    gripper_position += (z_axis * center_displacement_vec[2]);
    gripper_position += (x_axis * center_displacement_vec[0]);
    out_grasp_pose = Eigen::Translation3f(gripper_position) * orientation;
}
