#ifndef SIM_SPAWN_COLLISION_STRUCTURES_H_INCLUDED
#define SIM_SPAWN_COLLISION_STRUCTURES_H_INCLUDED

// ___Other CPP___
#include <nlohmann/json.hpp>

// ___PCL___
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace plugin_spawn_collision_items
{
  static const rclcpp::Logger LOGGER = rclcpp::get_logger("spawn_collision_items");
  using json = nlohmann::json;

  enum ReturnCode_e
  {
    SUCCESS,
    FAILURE,
  };

  struct ItemElement
  {
    int32_t id = 0;
    int32_t item_id = -1;
    std::string label;
    pcl::PointCloud<pcl::PointXYZ>::Ptr merged_ptcld;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cam1_ptcld;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cam2_ptcld;
    nlohmann::json primitive_shape{};
    nlohmann::json position{};
    nlohmann::json orientation{};
  };

  struct Item
  {
    int32_t id = 0;
    std::string item_id_hash{};
    std::string label{};
    std::vector<ItemElement> item_elements;
  };
}

#endif
