#ifndef FACTORY_SPAWN_METHODS_HPP
#define FACTORY_SPAWN_METHODS_HPP

#include <memory>
#include "spawn_methods/base_spawn_method.hpp"
#include "spawn_methods/spawn_primitive.hpp"
#include "spawn_methods/spawn_primitive_segments.hpp"
#include "spawn_methods/spawn_dish.hpp"
#include "spawn_methods/spawn_octomap.hpp"
// #include "spawn_broccoli.hpp"
namespace plugin_spawn_collision_items
{
    class FactorySpawnMethod
    {
    public:
        FactorySpawnMethod() = delete;
        virtual ~FactorySpawnMethod() = delete;
        static std::shared_ptr<BaseSpawnMethod> createSpawnMethod(const std::string &fit_method)
        {
            if (fit_method == "cylinder" ||
                fit_method == "box" ||
                fit_method == "sphere")
                return std::make_shared<SpawnPrimitive>();
            else if (fit_method == "cylindersegments" ||
                     fit_method == "cuttingboard")
                return std::make_shared<SpawnPrimitiveSegments>();
            else if (fit_method == "plate" ||
                     fit_method == "bowl")
                return std::make_shared<SpawnDish>();
            else if (fit_method == "octomap")
                return std::make_shared<SpawnOctomap>();
            else
                return nullptr;
        }
    };
}
#endif