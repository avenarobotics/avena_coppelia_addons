#ifndef SPAWN_OCTOMAP_HPP
#define SPAWN_OCTOMAP_HPP

#include "spawn_methods/base_spawn_method.hpp"
#include <helpers.hpp>
namespace plugin_spawn_collision_items
{
    class SpawnOctomap : public BaseSpawnMethod
    {
    public:
        SpawnOctomap();
        virtual ~SpawnOctomap();
        virtual int spawn(ItemElement &item_element) override;

    private:
        float _voxel_size;
    };
}
#endif
