#ifndef SPAWN_PRIMITIVE_SEGMENTS_HPP
#define SPAWN_PRIMITIVE_SEGMENTS_HPP

#include "spawn_methods/base_spawn_method.hpp"
#include "spawn_methods/spawn_primitive.hpp"
namespace plugin_spawn_collision_items
{
    class SpawnPrimitiveSegments : public BaseSpawnMethod
    {
    public:
        SpawnPrimitiveSegments();
        virtual ~SpawnPrimitiveSegments();
        virtual int spawn(ItemElement &item_element) override;
    };
}
#endif
