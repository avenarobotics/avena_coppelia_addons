#ifndef SPAWN_PRIMITIVE_HPP
#define SPAWN_PRIMITIVE_HPP

#include "spawn_methods/base_spawn_method.hpp"
namespace plugin_spawn_collision_items
{
    class SpawnPrimitive : public BaseSpawnMethod
    {
    public:
        SpawnPrimitive();
        virtual ~SpawnPrimitive();
        virtual int spawn(ItemElement &item_element) override;

    private:
        std::vector<float> _assignObjectSizes(json primitive_shape);
    };
}
#endif
