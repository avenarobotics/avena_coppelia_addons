#include "spawn_methods/spawn_octomap.hpp"
namespace plugin_spawn_collision_items
{
    SpawnOctomap::SpawnOctomap()
        : BaseSpawnMethod(), _voxel_size(0.01f)
    {
    }

    SpawnOctomap::~SpawnOctomap()
    {
    }

    int SpawnOctomap::spawn(ItemElement &item_element)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_merged(item_element.merged_ptcld);
        helpers::commons::passThroughFilter(pcl_merged, "z", 0.001, 2);

        std::vector<float> points(pcl_merged->points.size() * 3);
        for (size_t i = 0; i < pcl_merged->points.size(); ++i)
        {
            points[3 * i + 0] = pcl_merged->points[i].x;
            points[3 * i + 1] = pcl_merged->points[i].y;
            points[3 * i + 2] = pcl_merged->points[i].z;
        }

        simInt octree_handle = simCreateOctree(_voxel_size, 0, 0, nullptr);
        simSetObjectParent(octree_handle, _world_handle, false);
        simInsertVoxelsIntoOctree(octree_handle, 1, points.data(), points.size() / 3, nullptr, nullptr, nullptr);
        simSetModelProperty(octree_handle, sim_modelproperty_not_renderable);
        return octree_handle;
    }
}
