#include "spawn_methods/spawn_primitive_segments.hpp"
namespace plugin_spawn_collision_items
{
    SpawnPrimitiveSegments::SpawnPrimitiveSegments()
        : BaseSpawnMethod()
    {
    }

    SpawnPrimitiveSegments::~SpawnPrimitiveSegments()
    {
    }

    int SpawnPrimitiveSegments::spawn(ItemElement &item_element)
    {
        SpawnPrimitive spawn_primitive;
        // spawn_primitive.setLabelData(_labels_data);
        std::vector<int> group;
        // ItemElement item_element = item_elements[0];
        for (auto &[key, val] : item_element.primitive_shape.items())
        {
            ItemElement shape_element;
            shape_element.primitive_shape = item_element.primitive_shape[key];
            shape_element.position = item_element.position[key];
            shape_element.orientation = item_element.orientation[key];
            group.push_back(spawn_primitive.spawn(shape_element));
        }
        int group_handle = simGroupShapes(group.data(), group.size());
        return group_handle;
    }
}
