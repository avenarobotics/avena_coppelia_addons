#include "spawn_methods/spawn_dish.hpp"
namespace plugin_spawn_collision_items
{
    SpawnDish::SpawnDish()
        : BaseSpawnMethod()
    {
    }

    SpawnDish::~SpawnDish()
    {
    }

    int SpawnDish::spawn(ItemElement &item_element)
    {
        DishData dish_data = _readData(item_element);
        std::string dish_name = item_element.label;

        simInt dish_handle = _spawn_models_handles[dish_name];
        dish_handle = _copyObject(dish_handle);
        if (dish_handle == -1)
            return -1;

        _setObjectPosition(dish_handle, _world_handle, dish_data.dish_position);
        _setObjectQuaternion(dish_handle, _world_handle, dish_data.dish_orientation);

        return dish_handle;
    }

    SpawnDish::DishData SpawnDish::_readData(ItemElement &item_element)
    {
        DishData dish_data;
        dish_data.dish_position = Eigen::Vector3f(item_element.position["x"], item_element.position["y"], item_element.position["z"]);
        dish_data.dish_orientation = Eigen::Quaternionf(item_element.orientation["w"], item_element.orientation["x"], item_element.orientation["y"], item_element.orientation["z"]);
        return dish_data;
    }
}
