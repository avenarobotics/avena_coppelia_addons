#include "spawn_methods/spawn_primitive.hpp"
#include "helpers.hpp"

namespace plugin_spawn_collision_items
{
    SpawnPrimitive::SpawnPrimitive()
        : BaseSpawnMethod()
    {
    }

    SpawnPrimitive::~SpawnPrimitive()
    {
    }

    int SpawnPrimitive::spawn(ItemElement &item_element)
    {
        if (item_element.primitive_shape.empty())
            return -1;

        auto obj_size = _assignObjectSizes(item_element.primitive_shape);
        if (obj_size.empty())
            return -1;

        int shape_handle = -1;
        // It is faster to create box pure shape every time than copy model inside the scene
        if (item_element.primitive_shape["fit_method"] == "box")
            shape_handle = simCreatePureShape(_shape_type[item_element.primitive_shape["fit_method"]], 16, obj_size.data(), 1, nullptr);
        else
        {
            simInt shape_handle_to_copy = _spawn_models_handles[item_element.primitive_shape["fit_method"]];
            shape_handle = _copyObject(shape_handle_to_copy);
            if (shape_handle == -1)
                return -1;
            simScaleObject(shape_handle, obj_size[0], obj_size[1], obj_size[2], 0);
        }

        Eigen::Vector3f temp = Eigen::Vector3f(item_element.position["x"], item_element.position["y"], item_element.position["z"]);
        _setObjectPosition(shape_handle, _world_handle, temp);
        _setObjectQuaternion(shape_handle, _world_handle, Eigen::Quaternionf(item_element.orientation["w"], item_element.orientation["x"], item_element.orientation["y"], item_element.orientation["z"]));

        return shape_handle;
    }

    std::vector<float> SpawnPrimitive::_assignObjectSizes(json primitive_shape)
    {
        // Debug debug("_assignObjectSizes", _debug);
        int shape = _shape_type[primitive_shape["fit_method"]];
        switch (shape)
        {
        case 0:
        {
            std::vector<float> dims = {primitive_shape["dims"]["x"], primitive_shape["dims"]["y"], primitive_shape["dims"]["z"]};
            return dims;
        }
        case 1:
        {
            std::vector<float> dims = {primitive_shape["dims"]["radius"]};
            return {dims[0] * 2, dims[0] * 2, dims[0] * 2};
        }
        case 2:
        {
            std::vector<float> dims = {primitive_shape["dims"]["height"], primitive_shape["dims"]["radius"]};
            return {dims[1] * 2, dims[1] * 2, dims[0]};
        }
        default:
            std::cout << "unsupported shape type" << std::endl;
            break;
        }
        return {};
    }
}
