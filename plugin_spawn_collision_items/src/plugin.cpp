#include "plugin.h"

namespace plugin_spawn_collision_items
{
    SpawnCollisionItems::SpawnCollisionItems()
        : Node("spawn_collision_items"),
          _voxel_size(0.01),
          _debug(false),
          _parameters_read(false)
    {
    }

    SpawnCollisionItems::~SpawnCollisionItems()
    {
        if (_reading_parameters_thread.joinable())
            _reading_parameters_thread.join();
    };

    void SpawnCollisionItems::onStart()
    {
        if (!registerScriptStuff())
            throw std::runtime_error("script stuff initialization failed");

        setExtVersion("Plugin for spawning collision objects");
        setBuildDate(BUILD_DATE);

        helpers::commons::setLoggerLevel(get_logger(), "debug");
        // rclcpp::QoS latching_qos = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        // _sub = create_subscription<custom_interfaces::msg::OctomapFilter>(
        //     "octomap_filter", latching_qos,
        //     [this](const custom_interfaces::msg::OctomapFilter::SharedPtr input_data) {
        //         RCLCPP_INFO(get_logger(), "Message received");
        //         _input_data = input_data;
        //     });
        _sub_manager = std::make_shared<helpers::SubscriptionsManager>(get_node_topics_interface());
        _sub_manager->createSubscription(Topics::octomap.name, Topics::octomap.type);
        _sub_manager->createSubscription(Topics::items.name, Topics::items.type);
        _action_server = rclcpp_action::create_server<SpawnItems>(
            get_node_base_interface(),
            get_node_clock_interface(),
            get_node_logging_interface(),
            get_node_waitables_interface(),
            "spawn_collision_items",
            std::bind(&SpawnCollisionItems::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&SpawnCollisionItems::_handleCancel, this, std::placeholders::_1),
            std::bind(&SpawnCollisionItems::_handleAccepted, this, std::placeholders::_1));

        _reading_parameters_thread = std::thread([this]() {
            using namespace std::chrono_literals;
            while (rclcpp::ok())
            {
                std::this_thread::sleep_for(100ms);
                if (!_readParametersFromServer())
                    break;
            }
        });
    }

    void SpawnCollisionItems::onInstancePass(const sim::InstancePassFlags &flags, bool first)
    {
        rclcpp::spin_some(get_node_base_interface());
    }

    rclcpp_action::GoalResponse SpawnCollisionItems::_handleGoal(const rclcpp_action::GoalUUID & /*uuid*/, std::shared_ptr<const SpawnItems::Goal> /*goal*/)
    {
        RCLCPP_INFO(get_logger(), "Received goal request");
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse SpawnCollisionItems::_handleCancel(const std::shared_ptr<GoalHandleSpawnItems> /*goal_handle*/)
    {
        RCLCPP_INFO(get_logger(), "Goal canceled");
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void SpawnCollisionItems::_handleAccepted(const std::shared_ptr<GoalHandleSpawnItems> goal_handle)
    {
        RCLCPP_INFO(get_logger(), "Goal accepted");
        auto execute = [this](const std::shared_ptr<GoalHandleSpawnItems> goal_handle) -> void {
            helpers::Timer timer("Spawn collision items", get_logger());
            RCLCPP_INFO(get_logger(), "Executing goal");
            auto filtered_scene_octomap_msg = _sub_manager->getData<custom_interfaces::msg::FilteredSceneOctomap>(Topics::octomap.name);
            auto items_msg = _sub_manager->getData<custom_interfaces::msg::Items>(Topics::items.name);
            auto result = std::make_shared<SpawnItems::Result>();
            if (_validateInputs(filtered_scene_octomap_msg, items_msg))
            {
                RCLCPP_ERROR(get_logger(), "Invalid input data. Goal aborted...");
                goal_handle->abort(result);
                return;
            }

            RCLCPP_INFO(get_logger(), "main Loop.");

            if (_spawnCollisionItems(filtered_scene_octomap_msg, items_msg))
            {
                RCLCPP_ERROR(get_logger(), "Error occured while spawning collision items. Goal failed.");
                goal_handle->abort(result);
                return;
            }

            if (rclcpp::ok())
            {
                goal_handle->succeed(result);
                RCLCPP_INFO(get_logger(), "Goal succeeded");
            }
        };
        execute(goal_handle);
    }

    int SpawnCollisionItems::_validateInputs(const FilteredSceneOctomapMsg::SharedPtr &filtered_scene_octomap_msg, const ItemsMsg::SharedPtr &items_msg)
    {
        // {
        //     const std::lock_guard<std::mutex> lg(_parameters_server_mutex);
        if (!_parameters_read)
        {
            RCLCPP_WARN(get_logger(), "Labels are not loaded yet");
            return 1;
        }
        // }

        if (!filtered_scene_octomap_msg ||
            !items_msg ||
            filtered_scene_octomap_msg->header.stamp == builtin_interfaces::msg::Time() ||
            items_msg->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(get_logger(), "Invalid header in message.");
            return 1;
        }
        return 0;
    }

    int SpawnCollisionItems::_spawnCollisionOctomap(const FilteredSceneOctomapMsg::SharedPtr &filtered_scene_octomap_msg, int world_handle, float octomap_grid_size)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_scene_octomap(new pcl::PointCloud<pcl::PointXYZ>);
        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(filtered_scene_octomap_msg->filtered_scene_octomap, filtered_scene_octomap);

        // auto v = helpers::commons::visualize({filtered_scene_octomap}, {}, nullptr, "collision_octomap");

        std::vector<float> points(filtered_scene_octomap->points.size() * 3);
        for (size_t i = 0; i < filtered_scene_octomap->points.size(); ++i)
        {
            points[3 * i + 0] = filtered_scene_octomap->points[i].x;
            points[3 * i + 1] = filtered_scene_octomap->points[i].y;
            points[3 * i + 2] = filtered_scene_octomap->points[i].z;
        }

        simInt octree_handle = simCreateOctree(octomap_grid_size, 0, 0, nullptr);
        simSetObjectParent(octree_handle, world_handle, false);
        simInsertVoxelsIntoOctree(octree_handle, 1, points.data(), points.size() / 3, nullptr, nullptr, nullptr);
        simSetModelProperty(octree_handle, sim_modelproperty_not_renderable);
        simSetObjectName(octree_handle, "collision_octomap");
        return octree_handle;
    }

    int SpawnCollisionItems::_decodeRosMsg(const ItemsMsg::SharedPtr &items_msg, std::vector<Item> &items)
    {
        items.resize(items_msg->items.size());

        for (size_t i = 0; i < items_msg->items.size(); i++)
        {
            Item &item = items[i];
            ItemMsg &item_msg = items_msg->items[i];

            item.id = item_msg.id;
            item.label = item_msg.label;
            item.item_elements.resize(item_msg.item_elements.size());
            for (size_t el_idx = 0; el_idx < item_msg.item_elements.size(); el_idx++)
            {
                ItemElement &item_element = item.item_elements[el_idx];
                ItemElementMsg &item_element_msg = item_msg.item_elements[el_idx];

                item_element.id = item_element_msg.id;
                item_element.item_id = item_msg.id;
                item_element.label = item_element_msg.label;

                //PointClouds
                helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element_msg.merged_ptcld, item_element.merged_ptcld);
                helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element_msg.cam1_ptcld, item_element.cam1_ptcld);
                helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element_msg.cam2_ptcld, item_element.cam2_ptcld);

                // Primitive shape of estimation
                if (item_element_msg.primitive_shape != "")
                    item_element.primitive_shape = json::parse(item_element_msg.primitive_shape);
                // Position of estimation
                if (item_element_msg.position != "")
                    item_element.position = json::parse(item_element_msg.position);
                // Orientation of estimation
                if (item_element_msg.orientation != "")
                    item_element.orientation = json::parse(item_element_msg.orientation);
            }
        }
        RCLCPP_INFO(get_logger(), "msg converted succesfully");
        return 0;
    }

    int SpawnCollisionItems::_spawnCollisionItems(const FilteredSceneOctomapMsg::SharedPtr &filtered_scene_octomap_msg, const ItemsMsg::SharedPtr &items_msg)
    {
        Debug debug("SpawnCollisionItems", _debug);

        int world_handle = simGetObjectHandle("DummyWorld");

        std::string parent_name = "collisions_parent";
        int collisions_parent_handle = simGetObjectHandle(parent_name.c_str());
        simSetObjectName(collisions_parent_handle, parent_name.c_str());

        _clearScene(collisions_parent_handle);

        int collision_octomap_handle = _spawnCollisionOctomap(filtered_scene_octomap_msg, world_handle, _voxel_size);
        if (collision_octomap_handle == -1)
        {
            RCLCPP_ERROR(get_logger(), "Problem occured while spawning octomap.");
            return 1;
        }
        else
            RCLCPP_INFO(get_logger(), "Octomap spawned correctly.");
        simSetObjectParent(collision_octomap_handle, collisions_parent_handle, true);

        std::vector<Item> items;
        if (_decodeRosMsg(items_msg, items))
        {
            RCLCPP_ERROR(get_logger(), "Problem occured while decoding ROS topic message.");
            return 1;
        }

        for (auto &item : items)
        {
            // helpers::Timer timer("id: " + std::to_string(item.id) + ", label: " + item.label, get_logger());
            std::vector<int> elements_handles;
            for (auto &item_element : item.item_elements)
            {
                std::string spawn_fit_method = "octomap";
                try
                {
                    json primitive_shape = item_element.primitive_shape;
                    if (primitive_shape.contains("fit_method") && primitive_shape["fit_method"] == "octomap")
                        spawn_fit_method = primitive_shape["fit_method"];
                    else
                        spawn_fit_method = _default_fit_methods[item_element.label];
                }
                catch (const json::exception &e)
                {
                    RCLCPP_WARN_STREAM(get_logger(), "Invalid primitive shape format. Trying to spawn as OCTOMAP item element with ID: " << item_element.id << ", label: " << item_element.label);
                }

                auto spawn_method = FactorySpawnMethod::createSpawnMethod(spawn_fit_method);
                if (spawn_method == nullptr)
                {
                    RCLCPP_WARN_STREAM(get_logger(), "Invalid spawn method. Dropping spawning item element with ID: " << item_element.id << ", label: " << item_element.label);
                    continue;
                }
                int element_handle = spawn_method->spawn(item_element);
                elements_handles.push_back(element_handle);
            }
            int group_handle = simGroupShapes(elements_handles.data(), elements_handles.size());
            if (elements_handles.size() == 1)
                group_handle = elements_handles[0];
            else if (group_handle == -1)
            {
                group_handle = simCreateDummy(0.001, nullptr);
                for (auto &element_handle : elements_handles)
                    simSetObjectParent(element_handle, group_handle, true);
            }
            simSetObjectParent(group_handle, collisions_parent_handle, true);
            std::string object_name = item.label + "_" + std::to_string(item.id);
            simSetObjectName(group_handle, object_name.c_str());
            simSetObjectSpecialProperty(group_handle, sim_objectspecialproperty_collidable);
            const simInt visibility_layer = 1;
            simSetObjectInt32Parameter(group_handle, sim_objintparam_visibility_layer, visibility_layer);
        }
        return 0;
    }

    void SpawnCollisionItems::_removeCollisionItems()
    {
        std::string parent_name = "collisions_parent";
        int collisions_parent_handle = simGetObjectHandle(parent_name.c_str());
        _clearScene(collisions_parent_handle);
    }

    int SpawnCollisionItems::_clearScene(int &parent_handle)
    {
        Debug debug("_clearScene", _debug);
        if (parent_handle == -1)
        {
            parent_handle = simCreateDummy(0.001, nullptr);
            simSetObjectName(parent_handle, "collisions_parent");
        }
        int child_handle = simGetObjectChild(parent_handle, 0);
        while (child_handle != -1)
        {
            simRemoveObject(child_handle);
            child_handle = simGetObjectChild(parent_handle, 0);
        }
        return 0;
    }

    int SpawnCollisionItems::_readParametersFromServer()
    {
        RCLCPP_INFO_ONCE(get_logger(), "Reading parameters from the server");

        if (_parameters_read)
            return 0;

        using namespace std::chrono_literals;
        auto parameters_server_client = std::make_shared<rclcpp::AsyncParametersClient>(
            get_node_base_interface(),
            get_node_topics_interface(),
            get_node_graph_interface(),
            get_node_services_interface(), 
            "parameters_server");
        if (!parameters_server_client->wait_for_service(1s))
        {
            if (!rclcpp::ok())
            {
                RCLCPP_WARN(get_logger(), "Interrupted while waiting for the service. Exiting...");
                return 1;
            }
            RCLCPP_WARN_ONCE(get_logger(), "Parameters service not available. Trying to load parameters...");
            return 1;
        }
        RCLCPP_INFO(get_logger(), "Parameters service available...");

        std::shared_future<std::vector<rclcpp::Parameter>> future_result = parameters_server_client->get_parameters({"labels", "areas_parameters"});
        if (future_result.wait_for(1s) == std::future_status::timeout)
        {
            RCLCPP_ERROR(get_logger(), "Timeout when reading parameters...");
            return 1;
        }
        std::vector<rclcpp::Parameter> parameters = future_result.get();

        // Extract parameter values
        rclcpp::Parameter labels_parameter = parameters[0]; // we can safely access first element because we only request on parameter
        if (labels_parameter.get_type() != rclcpp::ParameterType::PARAMETER_STRING_ARRAY)
        {
            RCLCPP_ERROR(get_logger(), "Invalid type of \"labels\" parameter...");
            return 1;
        }
        rclcpp::Parameter areas_parameter = parameters[1]; // we can safely access second element because we only request on parameter
        if (areas_parameter.get_type() != rclcpp::ParameterType::PARAMETER_STRING)
        {
            RCLCPP_ERROR(get_logger(), "Invalid type of \"areas_parameters\" parameter...");
            return 1;
        }
        std::vector<std::string> labels = labels_parameter.as_string_array();
        std::string areas = areas_parameter.as_string();

        // {
        //     const std::lock_guard<std::mutex> lg(_parameters_server_mutex);
        if (_parseLabelsFromParametersServer(labels) || _parseOctomapVoxelSizeFromParametersServer(areas))
        {
            _default_fit_methods.clear();
            return 1;
        }
        // }
        _parameters_read = true;
        RCLCPP_INFO(get_logger(), "Parameters read successfully...");
        return 0;
    }

    int SpawnCollisionItems::_parseLabelsFromParametersServer(const std::vector<std::string> &labels)
    {
        RCLCPP_INFO(get_logger(), "Parsing \"labels\" parameter...");
        RCLCPP_INFO_STREAM(get_logger(), "Number of labels: " << labels.size());
        try
        {
            for (const auto &label : labels)
            {
                json label_data = json::parse(label);
                _default_fit_methods[label_data["label"]] = label_data["fit_method"];
            }
        }
        catch (const json::exception &e)
        {
            RCLCPP_ERROR_STREAM(get_logger(), "Error while parsing labels information. Dropping reading rest of parameters from the server");
            return 1;
        }
        RCLCPP_INFO(get_logger(), "Label data loaded successfully");
        return 0;
    }

    int SpawnCollisionItems::_parseOctomapVoxelSizeFromParametersServer(const std::string &areas)
    {
        RCLCPP_INFO(get_logger(), "Parsing \"octomap_voxel_size\" parameter...");
        try
        {
            json areas_json = json::parse(areas);
            _voxel_size = areas_json["octomap_voxel_size"];
        }
        catch (const json::exception &e)
        {
            RCLCPP_ERROR_STREAM(get_logger(), "Exception with ID: " << e.id << "; message: " << e.what());
            return 1;
        }
        RCLCPP_INFO(get_logger(), "Octomap voxel size loaded successfully");
        return 0;
    }
}
SIM_PLUGIN(PLUGIN_NAME, PLUGIN_VERSION, plugin_spawn_collision_items::SpawnCollisionItems)
#include "stubsPlusPlus.cpp"
