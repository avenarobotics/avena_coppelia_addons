#ifndef UPDATE_ROBOT_STATE_COPPELIA_PLUGIN_HPP
#define UPDATE_ROBOT_STATE_COPPELIA_PLUGIN_HPP

// ___CPP___
#include <thread>

// ___Coppelia___
#include "config.h"
#include "simPlusPlus/Plugin.h"
#include "simPlusPlus/Handle.h"
#include "stubs.h"
#include <vector>
// ___ROS2___
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/joint_state.hpp>

#define PLUGIN_NAME "ROS2PluginUpdateRobotState"
#define PLUGIN_VERSION 1

namespace update_robot_state
{
  class UpdateRobotState : public sim::Plugin, rclcpp::Node
  {
  public:
    UpdateRobotState();
    virtual ~UpdateRobotState();
    void onStart() override;
    void setCurrentRobotPosition(setCurrentRobotPosition_in *in, setCurrentRobotPosition_out *out);

  private:
    std::vector<float> _joint_position_all;
    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr _joint_state_sub;
    bool _flag_joint_size{false};
    std::vector<float> _joint_position_temp;
  };
} // namespace update_robot_state

#endif
