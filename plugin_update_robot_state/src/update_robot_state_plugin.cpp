#include "update_robot_state_plugin.hpp"

namespace update_robot_state
{
    UpdateRobotState::UpdateRobotState()
        : Node("update_robot_state")
    {
    }

    UpdateRobotState::~UpdateRobotState()
    {
    }

    void UpdateRobotState::onStart()
    {
        if (!registerScriptStuff())
            throw std::runtime_error("script stuff initialization failed");

        setExtVersion("Avena Robotics - Update robot state plugin");
        setBuildDate(BUILD_DATE);

        auto qos_latching = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        auto joint_states_callback = [this](const sensor_msgs::msg::JointState::SharedPtr joint_states) {
            for (size_t joint_idx = 0; joint_idx < joint_states->name.size(); ++joint_idx)
            {
                if (joint_states->name.size() == 7 && _flag_joint_size == false)
                {
                    _joint_position_temp.push_back(joint_states->position[joint_idx]);
                    if (joint_idx == joint_states->name.size() - 1)
                        _flag_joint_size = true;
                }
                else if (joint_states->name.size() == 3 && _flag_joint_size == true)
                {
                    _joint_position_temp.push_back(joint_states->position[joint_idx]);
                    if (joint_idx == joint_states->name.size() - 1)
                        _flag_joint_size = false;
                }
            }

            if (_joint_position_temp.size() == 10)
            
            {   _joint_position_all.clear();
                _joint_position_all.resize(10);
                _joint_position_all = _joint_position_temp;
                _joint_position_temp.clear();
            }
            if (_joint_position_temp.size() >= 10)
                _joint_position_temp.clear();
        };

        _joint_state_sub = create_subscription<sensor_msgs::msg::JointState>("joint_states", qos_latching, joint_states_callback);

        // Spinner thread to spin callback as frequently as possible without waiting for Coppelia to call onInstancePass
        std::thread([this]() {
            using namespace std::chrono_literals;
            while (rclcpp::ok())
            {
                rclcpp::spin_some(get_node_base_interface());
                std::this_thread::sleep_for(1ms);
            }
        }).detach();
    }

    void UpdateRobotState::setCurrentRobotPosition(setCurrentRobotPosition_in *in, setCurrentRobotPosition_out *out)
    {
        out->joint_positions = _joint_position_all;
        _joint_position_all.clear();
    }

} // namespace update_robot_state

SIM_PLUGIN(PLUGIN_NAME, PLUGIN_VERSION, update_robot_state::UpdateRobotState)
#include "stubsPlusPlus.cpp"
