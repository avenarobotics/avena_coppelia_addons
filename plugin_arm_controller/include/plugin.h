#ifndef SIM_ARM_CONTROLLER_H
#define SIM_ARM_CONTROLLER_H

// ___COPPELIA PLUGINS___
#include "config.h"
#include "simPlusPlus/Plugin.h"
#include "stubs.h"

// ___CPP___
#include <memory>
#include <string>
#include <chrono>
#include <thread>

//___ROS___
#include <rclcpp/rclcpp.hpp>
#include <rclcpp/time.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <trajectory_msgs/msg/joint_trajectory.hpp>

#include <rclcpp_action/rclcpp_action.hpp>
#include <custom_interfaces/action/simple_action.hpp>

#define PLUGIN_NAME "ArmController"
#define PLUGIN_VERSION 1

namespace arm_controller
{
  class ArmController : public sim::Plugin
  {
  public:
    using ExecuteMove = custom_interfaces::action::SimpleAction;
    using GoalHandleExecuteMove = rclcpp_action::ServerGoalHandle<ExecuteMove>;

    ArmController() {};
    virtual ~ArmController(){};
    void onStart() override;
    void onInstancePass(const sim::InstancePassFlags &flags, bool first) override;
    void broadcastJoinstState();

  private:
    rclcpp::Subscription<trajectory_msgs::msg::JointTrajectoryPoint>::SharedPtr _sub_trajectory_point;
    rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_joint_state;

    rclcpp::TimerBase::SharedPtr _timer;
    rclcpp::Node::SharedPtr _node;

    // std::vector<int> _joint_handles;
    // std::vector<std::string> _joint_names;

    void _setJointStateCallback(const trajectory_msgs::msg::JointTrajectoryPoint::SharedPtr trajectory_point_msg);
    void _broadcastJointState();

  };
} // namespace arm_controller

#endif